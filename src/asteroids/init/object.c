#include "../object/star.h"
#include "../object/spaceship.h"
#include "../object/asteroid.h"

#include "object.h"

void $c(Asteroids, initObjectEntries)()
{
    $c(ObjectInitializer, initAsteroidsStar)();
    $c(ObjectInitializer, initAsteroidsSpaceship)();
    $c(ObjectInitializer, initAsteroidsAsteroid)();
}
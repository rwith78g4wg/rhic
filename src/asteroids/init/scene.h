#ifndef ASTEROIDS_INIT_SCENE_H
#define ASTEROIDS_INIT_SCENE_H

#include "../../rhic/common/macro.h"

$$c(Context);

void $c(Asteroids, initScenes)($$c(Context) *context);

#endif //ASTEROIDS_INIT_SCENE_H

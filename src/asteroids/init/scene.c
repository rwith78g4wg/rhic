#include <math.h>

#include "../config.h"
#include "../init.h"
#include "../../rhic/common/context.h"
#include "../../rhic/common/eventhandler.h"
#include "../../rhic/common/object.h"
#include "../../rhic/common/scene.h"
#include "../../rhic/common/color.h"

#include "scene.h"


/**
 * Calculates the cross product of two vectors
 *
 * @param aX First vector X
 * @param aY First vector Y
 * @param aZ First vector Z
 * @param bX Second vector X
 * @param bY Second vector Y
 * @param bZ Second vector Z
 * @return Cross product
 */

float *$c(Inline, cross)(
    float aX,
    float aY,
    float aZ,

    float bX,
    float bY,
    float bZ
)
{
    alloc_array(float, c, 3);
    c[0] = aY * bZ - bY * aZ;
    c[1] = aZ * bX - bZ * aX;
    c[2] = aX * bY - bX * aY;
    return c;
}


/**
 * Calculates the dot product of two vectors
 *
 * @param aX First vector X
 * @param aY First vector Y
 * @param aZ First vector Z
 * @param bX Second vector X
 * @param bY Second vector Y
 * @param bZ Second vector Z
 * @return Dot product
 */

float $c(Inline, dot)(
    float aX,
    float aY,
    float aZ,

    float bX,
    float bY,
    float bZ
)
{
    return aX * bX + aY * bY + aZ * bZ;
}


/**
 * Normalizes vector
 *
 * @param size Size of vector array
 * @param a Vector pointer
 */

void $c(Inline, normalize)(
    int size,
    float *a
)
{
    float max = 0;
    {
        $aeach(float, i, a, 0, size) {
            if (*i > 0) {
                if (max < *i) {
                    max = *i;
                }
            } else {
                if (max < -*i) {
                    max = -*i;
                }
            }
        }
    }
    if (max > 0 && max != 1) {
        $aeach(float, i, a, 0, size) {
            *i /= max;
        }
    }
}


/**
 * Setups view matrix simulating camera position and three angles
 *
 * @param matrix Matrix to be modified
 * @param eyeX Eye position X
 * @param eyeY Eye position Y
 * @param eyeZ Eye position Z
 * @param pitch Rotation of camera by X
 * @param yaw Rotation of camera by Y
 * @param roll Rotation of camera by Z
 */

void $c(Inline, setLookAtO)(
    float matrix[16],

    float eyeX,
    float eyeY,
    float eyeZ,

    float pitch,
    float yaw,
    float roll
)
{
    float pitchSin = sin(pitch);
    float pitchCos = cos(pitch);
    float yawSin = sin(yaw);
    float yawCos = cos(yaw);
    float rollSin = sin(roll);
    float rollCos = cos(roll);

    matrix[0] = yawCos * rollCos;
    matrix[1] = pitchCos * rollSin + pitchSin * yawSin * rollCos;
    matrix[2] = pitchSin * rollSin - pitchCos * yawSin * rollCos;
    matrix[3] = 0;

    matrix[4] = -yawCos * rollSin;
    matrix[5] = pitchCos * rollCos - pitchSin * yawSin * rollSin;
    matrix[6] = pitchSin * rollCos + pitchCos * yawSin * rollSin;
    matrix[7] = 0;

    matrix[8] = yawSin;
    matrix[9] = -pitchSin * yawCos;
    matrix[10] = pitchCos * yawCos;
    matrix[11] = 0;

    matrix[12] = eyeX * matrix[0] + eyeZ * matrix[4] + eyeZ * matrix[8];
    matrix[13] = eyeX * matrix[1] + eyeY * matrix[5] + eyeZ * matrix[9];
    matrix[14] = eyeX * matrix[2] + eyeY * matrix[6] + eyeZ * matrix[10];
    matrix[15] = 1;
}


void *$c(Inline, updateScissor)(int argc, void **argv)
{
    if (argc < 2) {
        return NULL;
    }

    $$c(Scene) *scene = argv[0];
    $$c(Context) *context = $c(ContextSet, getContext)($_c(Asteroids, contextSet), "default");

    if (scene->scissor) {
        float aspect = (float)context->height / context->width;

        if (aspect < ASTEROIDS_ASPECT_RATIO) {
            int width = context->height / ASTEROIDS_ASPECT_RATIO;
            int letterBox = (context->width - width) / 2;
            scene->scissor->left = letterBox;
            scene->scissor->right = letterBox + width;
            scene->scissor->bottom = 0;
            scene->scissor->top = context->height;
        } else
        if (aspect > ASTEROIDS_ASPECT_RATIO) {
            int height = context->width * ASTEROIDS_ASPECT_RATIO;
            int letterBox = (context->height - height) / 2;
            scene->scissor->left = 0;
            scene->scissor->right = context->width;
            scene->scissor->bottom = letterBox;
            scene->scissor->top = letterBox + height;
        } else {
            scene->scissor->left = 0;
            scene->scissor->right = context->width;
            scene->scissor->bottom = 0;
            scene->scissor->top = context->height;
        }
    }

    return NULL;
}


/**
 * Changes scene view matrix. The handler is called on context initialization.
 *
 * @param argc
 * @param argv
 * @return
 */

float $c(Inline, angleX) = 0,
    $c(Inline, angleY) = 0;

void *$c(Inline, setSceneView)(int argc, void **argv)
{
    if (argc < 1) {
        return NULL;
    }

    $$c(Scene) *scene = argv[0];
    $$c(Context) *context = $c(ContextSet, getContext)($_c(Asteroids, contextSet), "default");

    $c(Inline, setLookAtO)(scene->viewMatrix, 0, 0, -1, 0, 0, 0);

    return NULL;
}

void $c(Inline, initLetterboxScene)($$c(Context) *context)
{
    $$c(Scene) *scene = $c(Scene, create)();

    scene->background = new_array(float, 4);
    RGBA(scene->background, 0.01, 0.01, 0.01, 1);

    $c(SceneSet, addScene)(context->sceneSet, "letterbox", scene);
}

void $c(Inline, initBackgroundScene)($$c(Context) *context)
{
    $$c(Scene) *scene = $c(Scene, create)();

    scene->scissor = new($$c(Rect));

    scene->background = new_array(float, 4);
    RGBA(scene->background, 0, 0, 0.05, 1);

    $c(EventHandler, register)(
        scene->eventHandlerGroups,
        RHIC_SCENE_EVENT_TYPE_REFRESH_VIEW,
        $c(Inline, setSceneView),
        0,
        NULL
    );
    $c(Scene, fireEvent)(&scene, RHIC_SCENE_EVENT_TYPE_REFRESH_VIEW, 0, NULL);
    $c(EventHandler, register)(
        scene->eventHandlerGroups,
        RHIC_SCENE_EVENT_TYPE_REFRESH_PROJECTION,
        $c(Inline, updateScissor),
        0,
        NULL
    );
    $c(Scene, fireEvent)(&scene, RHIC_SCENE_EVENT_TYPE_REFRESH_PROJECTION, 0, NULL);

    for (int i = 0; i < 64; i++) {
        $$c(Object) *star = $c(Object, create)("star", 0, NULL);

        $p(Scene, addObject)(scene, star);
    }

    $c(SceneSet, addScene)(context->sceneSet, "background", scene);
}


void $c(Inline, initSpaceshipsScene)($$c(Context) *context)
{
    $$c(Scene) *scene = $c(Scene, create)();

    scene->scissor = new($$c(Rect));

    $c(EventHandler, register)(
        scene->eventHandlerGroups,
        RHIC_SCENE_EVENT_TYPE_REFRESH_VIEW,
        $c(Inline, setSceneView),
        0,
        NULL
    );
    $c(Scene, fireEvent)(&scene, RHIC_SCENE_EVENT_TYPE_REFRESH_VIEW, 0, NULL);
    $c(EventHandler, register)(
        scene->eventHandlerGroups,
        RHIC_SCENE_EVENT_TYPE_REFRESH_PROJECTION,
        $c(Inline, updateScissor),
        0,
        NULL
    );
    $c(Scene, fireEvent)(&scene, RHIC_SCENE_EVENT_TYPE_REFRESH_PROJECTION, 0, NULL);

    $$c(Object) *spaceship = $c(Object, create)("spaceship", 0, NULL);

    $p(Scene, addObject)(scene, spaceship);

    $c(SceneSet, addScene)(context->sceneSet, "spaceships", scene);
}


void $c(Inline, initAsteroidsScene)($$c(Context) *context)
{
    $$c(Scene) *scene = $c(Scene, create)();

    scene->scissor = new($$c(Rect));

    $c(EventHandler, register)(
        scene->eventHandlerGroups,
        RHIC_SCENE_EVENT_TYPE_REFRESH_VIEW,
        $c(Inline, setSceneView),
        0,
        NULL
    );
    $c(Scene, fireEvent)(&scene, RHIC_SCENE_EVENT_TYPE_REFRESH_VIEW, 0, NULL);
    $c(EventHandler, register)(
        scene->eventHandlerGroups,
        RHIC_SCENE_EVENT_TYPE_REFRESH_PROJECTION,
        $c(Inline, updateScissor),
        0,
        NULL
    );
    $c(Scene, fireEvent)(&scene, RHIC_SCENE_EVENT_TYPE_REFRESH_PROJECTION, 0, NULL);

    $$c(Object) *asteroid = $c(Object, create)("asteroid", 0, NULL);

    $p(Scene, addObject)(scene, asteroid);

    $c(SceneSet, addScene)(context->sceneSet, "asteroids", scene);
}


void $c(Asteroids, initScenes)($$c(Context) *context)
{
    $c(Inline, initLetterboxScene)(context);
    $c(Inline, initBackgroundScene)(context);
    $c(Inline, initSpaceshipsScene)(context);
    $c(Inline, initAsteroidsScene)(context);
}
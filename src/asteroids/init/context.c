#include "../config.h"

#if ASTEROIDS_RANDOM
#include <time.h>
#endif

#include "../callback.h"
#include "../../rhic/common/context.h"
#include "../../rhic/common/pointer.h"
#include "../../rhic/gl/renderer.h"

#include "context.h"

int $c(Asteroids, createContext)()
{
#if ASTEROIDS_RANDOM
    srand(time(NULL));
#endif
    $$c(ContextProcessor) proc = {
        .render_init    = &$g(PlatformSpecific, init),
        .render_free    = &$g(Core, free),
        .render         = &$g(Asteroids, onRender),
        .process        = &$p(Asteroids, onProcess),
        .process_init   = &$p(Asteroids, onProcessInit),
        .process_free   = &$p(Asteroids, onProcessFree),
        .key_down       = NULL,
        .key_up         = NULL,
        .resize         = &$p(Asteroids, onResize),
        .pointer_down   = &$p(Asteroids, onPointerDown),
        .pointer_up     = &$p(Asteroids, onPointerUp)
    };
    return $c(Context, create)(0, $c(UPoint, create)(640, 360), "A S T E R O I D S\0", proc);
}
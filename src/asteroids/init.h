#ifndef ASTEROIDS_INIT_H
#define ASTEROIDS_INIT_H

#include "../rhic/common/macro.h"

$$c(List);

$$c(List) *$_c(Asteroids, contextSet);

#include "init/context.h"
#include "init/object.h"
#include "init/scene.h"

#endif //ASTEROIDS_INIT_H

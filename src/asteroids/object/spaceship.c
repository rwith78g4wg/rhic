#include <math.h>

#include "../../rhic/gl/texture.h"
#include "../../rhic/gl/shader.h"
#include "../../rhic/gl/entity.h"
#include "../../rhic/common/list.h"
#include "../../rhic/common/color.h"
#include "../../rhic/common/context.h"
#include "../../rhic/common/object.h"
#include "../../rhic/common/eventhandler.h"
#include "../../rhic/common/pointer.h"
#include "../init.h"

#include "spaceship.h"

#define SPACESHIP_ARG_X 0
#define SPACESHIP_ARG_Y 1
#define SPACESHIP_ARG_SX 2
#define SPACESHIP_ARG_SY 3
#define SPACESHIP_ARG_LAST_SX 4
#define SPACESHIP_ARG_LAST_SY 5
#define SPACESHIP_ARG_COLOR 6

#define SPACESHIP_CONTROLABILITY 5

void *$g(Inline, onAsteroidsSpaceshipCreate)(int argc, void **argv)
{
    if (argc < 1) {
        return 0;
    }
    $$c(Object) *spaceship = argv[0];
    spaceship->staticEntitiesNum = 2;
    spaceship->staticEntities = new_array($$g(Entity), 2);

    float vertices[] = {
            0, 0, -3, -3, 0,
            0, 1, -3, 3, 0,
            1, 0, 3, -3, 0,
            1, 1, 3, 3, 0,
    };

    int indices[] = {
            0, 1, 2, 1, 2, 3
    };

    $$g(Entity) *entity = new($$g(Entity));
    RGBA(entity->color, 1, 1, 1, 1);
    entity->vertices = new_array(float, 20);
    entity->vertices_size = 4;
    copy(vertices, entity->vertices);
    entity->indices = new_array(int, 6);
    entity->indices_size = 6;
    copy(indices, entity->indices);

    $g(Entity, init)(entity);

    spaceship->staticEntities[0] = entity;

    $g(Entity, init)(entity);

    int outlineIndices[] = {
        0, 1, 1, 3, 3, 2, 2, 0
    };

    entity = new($$g(Entity));
    RGBA(entity->color, 1, 0, 0, 1);
    entity->vertices = new_array(float, 20);
    entity->vertices_size = 4;
    copy(vertices, entity->vertices);
    entity->indices = new_array(int, 8);
    entity->indices_size = 8;
    copy(outlineIndices, entity->indices);

    $g(Entity, init)(entity);
    entity->stroke = 1;

    spaceship->staticEntities[1] = entity;

    return 0;
}



void $c(Inline, onAsteroidsSpaceshipCreate)(int argc, void **argv)
{
    if (argc < 1) {
        return;
    }
    $$c(Object) *ship = argv[0];
    ship->argc = 7;
    ship->argv = new_array(void*, 7);
    $aeach(void*, arg, ship->argv, 0, 6) {
        *arg = new(float);
        *(float*)*arg = 0;
    }
    ship->argv[6] = new(unsigned char);
    *(unsigned char*)ship->argv[6] = rand() % 8;

    void **nargv = new(void*);
    *nargv = *argv;

    $g(Call, onRender)($g(Inline, onAsteroidsSpaceshipCreate), 1, nargv);
}


void $p(Inline, processSpaceshipControls)($$c(Object) *ship)
{
    $$c(Context) *context = $c(ContextSet, getContext)($_c(Asteroids, contextSet), "default");
    if (context->keys['S']) {
        (*(float*)ship->argv[SPACESHIP_ARG_SY])--;
    }
    if (context->keys['W']) {
        (*(float*)ship->argv[SPACESHIP_ARG_SY])++;
    }
    if (context->keys['A']) {
        (*(float*)ship->argv[SPACESHIP_ARG_SX])--;
    }
    if (context->keys['D']) {
        (*(float*)ship->argv[SPACESHIP_ARG_SX])++;
    }
}

void $p(Inline, processSpaceshipMovement)($$c(Object) *ship)
{
    *(float*)ship->argv[SPACESHIP_ARG_X] += *(float*)ship->argv[SPACESHIP_ARG_SX] * (0.02 + 0.02 * SPACESHIP_CONTROLABILITY);
    *(float*)ship->argv[SPACESHIP_ARG_Y] += *(float*)ship->argv[SPACESHIP_ARG_SY] * (0.02 + 0.02 * SPACESHIP_CONTROLABILITY);

    *(float*)ship->argv[SPACESHIP_ARG_SX] *= 1 - SPACESHIP_CONTROLABILITY * 0.02 - 0.01;
    *(float*)ship->argv[SPACESHIP_ARG_SY] *= 1 - SPACESHIP_CONTROLABILITY * 0.02 - 0.01;

    if (*(float*)ship->argv[SPACESHIP_ARG_SX] != 0) {
        *(float*)ship->argv[SPACESHIP_ARG_LAST_SX] = *(float*)ship->argv[SPACESHIP_ARG_SX];
    }
    if (*(float*)ship->argv[SPACESHIP_ARG_SY] != 0) {
        *(float*)ship->argv[SPACESHIP_ARG_LAST_SY] = *(float*)ship->argv[SPACESHIP_ARG_SY];
    }

    if (*(float*)ship->argv[SPACESHIP_ARG_SX] > -0.05 * SPACESHIP_CONTROLABILITY && *(float*)ship->argv[SPACESHIP_ARG_SX] < 0.05 * SPACESHIP_CONTROLABILITY
     && *(float*)ship->argv[SPACESHIP_ARG_SY] > -0.05 * SPACESHIP_CONTROLABILITY && *(float*)ship->argv[SPACESHIP_ARG_SY] < 0.05 * SPACESHIP_CONTROLABILITY) {
        *(float*)ship->argv[SPACESHIP_ARG_SX] = 0;
        *(float*)ship->argv[SPACESHIP_ARG_SY] = 0;
    }

    if (*(float*)ship->argv[SPACESHIP_ARG_X] > 71.1111) {
        *(float*)ship->argv[SPACESHIP_ARG_X] -= 142.2222;
    }

    if (*(float*)ship->argv[SPACESHIP_ARG_X] < -71.1111) {
        *(float*)ship->argv[SPACESHIP_ARG_X] += 142.2222;
    }

    if (*(float*)ship->argv[SPACESHIP_ARG_Y] > 40) {
        *(float*)ship->argv[SPACESHIP_ARG_Y] -= 80;
    }

    if (*(float*)ship->argv[SPACESHIP_ARG_Y] < -40) {
        *(float*)ship->argv[SPACESHIP_ARG_Y] += 80;
    }
}


void $p(Inline, onAsteroidsSpaceshipProcess)(int argc, void **argv)
{
    if (argc < 1) {
        return;
    }
    $$c(Object) *ship = argv[0];
    $p(Inline, processSpaceshipControls)(ship);
    $p(Inline, processSpaceshipMovement)(ship);
}



void $g(Inline, onAsteroidsSpaceshipRender)(int argc, void **argv)
{
    if (argc < 3 || !argv[1] || !argv[2]) {
        return;
    }
    $$c(Object) *ship = argv[0];
    if (ship->staticEntitiesNum < 2) {
        return;
    }

    ship->staticEntities[1]->translate[0] = ship->staticEntities[0]->translate[0] = *(float*)ship->argv[SPACESHIP_ARG_X];
    ship->staticEntities[1]->translate[1] = ship->staticEntities[0]->translate[1] = *(float*)ship->argv[SPACESHIP_ARG_Y];

    ship->staticEntities[1]->rotate[2] = ship->staticEntities[0]->rotate[2] = 1;
    ship->staticEntities[1]->rotate[3] = ship->staticEntities[0]->rotate[3] = atan2(*(float*)ship->argv[SPACESHIP_ARG_LAST_SX], *(float*)ship->argv[SPACESHIP_ARG_LAST_SY]);

    $g(Entity, render)(ship->staticEntities[0], $g(Shader, get)("simple"), $g(Texture, getFromRhg)("spaceship", *(unsigned char*)ship->argv[SPACESHIP_ARG_COLOR]), argv[1], argv[2]);
    //$g(Entity, render)(ship->staticEntities[1], $g(Shader, get)("simple"), NULL, argv[1], argv[2]);
}

void $c(ObjectInitializer, initAsteroidsSpaceship)()
{
    $$c(List)* eventHandlerGroups = $c(List, create)();
    $c(EventHandler, register)(
        eventHandlerGroups,
        RHIC_OBJECT_EVENT_TYPE_CREATE,
        (void*)$c(Inline, onAsteroidsSpaceshipCreate),
        0,
        NULL
    );
    $c(EventHandler, register)(
        eventHandlerGroups,
        RHIC_OBJECT_EVENT_TYPE_PROCESS,
        (void*)$p(Inline, onAsteroidsSpaceshipProcess),
        0,
        NULL
    );
    $c(EventHandler, register)(
        eventHandlerGroups,
        RHIC_OBJECT_EVENT_TYPE_RENDER,
        (void*)$g(Inline, onAsteroidsSpaceshipRender),
        0,
        NULL
    );

    $c(Object, registerEntry)("spaceship", eventHandlerGroups);
}
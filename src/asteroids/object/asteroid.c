#include <math.h>

#include "../../rhic/gl/texture.h"
#include "../../rhic/gl/shader.h"
#include "../../rhic/gl/entity.h"
#include "../../rhic/common/list.h"
#include "../../rhic/common/color.h"
#include "../../rhic/common/context.h"
#include "../../rhic/common/object.h"
#include "../../rhic/common/scene.h"
#include "../../rhic/common/eventhandler.h"
#include "../../rhic/common/pointer.h"
#include "../init.h"

#include "spaceship.h"

#define ASTEROID_ARG_X 0
#define ASTEROID_ARG_Y 1
#define ASTEROID_ARG_SX 2
#define ASTEROID_ARG_SY 3
#define ASTEROID_ARG_SIZE 4

void *$g(Inline, onAsteroidsAsteroidCreate)(int argc, void **argv)
{
    if (argc < 1) {
        return 0;
    }
    unsigned char asteroidSize = *(unsigned char*)argv[1];
    free(argv[1]);

    $$c(Object) *asteroid = argv[0];
    asteroid->staticEntitiesNum = 3;
    asteroid->staticEntities = new_array($$g(Entity), 3);

    unsigned char sides = rand() % (asteroidSize + 1) + asteroidSize * 2 + 3;

    $$g(Entity) *entity = new($$g(Entity));
    RGBA(entity->color, 0.25 - rand() % 10 * 0.005, 0.225 - rand() % 10 * 0.002, 0.2 - rand() % 10 * 0.001, 1);

    entity->vertices = new_array(float, sides * 15);
    entity->vertices_size = sides * 3;
    entity->indices = new_array(int, sides * 9 - 6);
    entity->indices_size = sides * 9 - 6;

    float anglePs = 6.2831853 / sides;
    float size = pow(asteroidSize, 1.5) * 2 + 2;
    float craterSize = size - 0.25;
    float craterAngle = 6.2831853 / (15 * pow(asteroidSize, 0.95));

    for (unsigned char i = 0; i < sides; i++) {
        unsigned int i5 = i * 5;
        entity->vertices[i5] = 0;
        entity->vertices[i5 + 1] = 0;
        entity->vertices[i5 + 2] = cos(i * anglePs) * craterSize;
        entity->vertices[i5 + 3] = sin(i * anglePs) * craterSize;
        entity->vertices[i5 + 4] = 0;
    }

    alloc_array(float, craterRatio, sides);

    {
        $aeach (float, ratio, craterRatio, 0, sides) {
            *ratio = 0.5 + rand() % 100 * 0.01;
        }
    }

    for (unsigned char i = 0; i < sides; i++) {
        unsigned int i3 = (i * 2 + sides) * 5;

        float thisCraterAngle = craterAngle * craterRatio[i];
        float thisCraterSize = size * ((craterRatio[i] - 0.5) * 0.1 + 1);

        entity->vertices[i3] = 0;
        entity->vertices[i3 + 1] = 0;
        entity->vertices[i3 + 2] = cos(i * anglePs + thisCraterAngle) * thisCraterSize;
        entity->vertices[i3 + 3] = sin(i * anglePs + thisCraterAngle) * thisCraterSize;
        entity->vertices[i3 + 4] = 0;

        unsigned char index = (i + 1) % sides;
        thisCraterAngle = craterAngle * craterRatio[index];
        thisCraterSize = size * ((craterRatio[index] - 0.5) * 0.1 + 1);

        entity->vertices[i3 + 5] = 0;
        entity->vertices[i3 + 6] = 0;
        entity->vertices[i3 + 7] = cos(i * anglePs + anglePs - thisCraterAngle) * thisCraterSize;
        entity->vertices[i3 + 8] = sin(i * anglePs + anglePs - thisCraterAngle) * thisCraterSize;
        entity->vertices[i3 + 9] = 0;

        i3 = i * 6 + sides * 3 - 6;

        unsigned int en = i * 2 + sides;

        entity->indices[i3] = i;
        entity->indices[i3 + 1] = en;
        entity->indices[i3 + 2] = en + 1;

        entity->indices[i3 + 3] = i;
        entity->indices[i3 + 4] = en + 1;
        entity->indices[i3 + 5] = (i + 1) % sides;
    }

    for (unsigned char i = 2; i < sides; i++) {
        unsigned int i3 = i * 3 - 6;

        entity->indices[i3] = 0;
        entity->indices[i3 + 1] = i - 1;
        entity->indices[i3 + 2] = i;
    }

    $g(Entity, init)(entity);

    asteroid->staticEntities[0] = entity;

    unsigned char craterResolution = asteroidSize + 4;
    unsigned int craterNum = (int)pow(asteroidSize * 2, 1.5) + rand() % 3;
    entity = new($$g(Entity));
    entity->color[3] = 1;
    $c(Color, brightenBy)(asteroid->staticEntities[0]->color, entity->color, 0.625);
    $c(Color, saturateBy)(entity->color, entity->color, 0.25);

    entity->vertices_size = sides * 6 + craterNum * craterResolution;
    entity->vertices = new_array(float, entity->vertices_size * 5);
    entity->indices_size = sides * 12 + craterNum * (craterResolution - 2) * 3;
    entity->indices = new_array(int, entity->indices_size);

    for (unsigned char i = 0; i < sides; i++) {
        unsigned int i5 = i * 30;
        float thisCraterAngle = craterAngle * craterRatio[i];
        float thisCraterSize = size * ((craterRatio[i] - 0.5) * 0.1 + 1);
        float antiCraterSize = craterSize * 1.5 - thisCraterSize * 0.5;

        entity->vertices[i5] = 0;
        entity->vertices[i5 + 1] = 0;
        entity->vertices[i5 + 2] = cos(i * anglePs - thisCraterAngle * 0.5) * antiCraterSize;
        entity->vertices[i5 + 3] = sin(i * anglePs - thisCraterAngle * 0.5) * antiCraterSize;
        entity->vertices[i5 + 4] = 0;

        entity->vertices[i5 + 5] = 0;
        entity->vertices[i5 + 6] = 0;
        entity->vertices[i5 + 7] = cos(i * anglePs - thisCraterAngle) * thisCraterSize;
        entity->vertices[i5 + 8] = sin(i * anglePs - thisCraterAngle) * thisCraterSize;
        entity->vertices[i5 + 9] = 0;

        entity->vertices[i5 + 10] = 0;
        entity->vertices[i5 + 11] = 0;
        entity->vertices[i5 + 12] = cos(i * anglePs - thisCraterAngle * 0.5) * (thisCraterSize + craterSize) / 2;
        entity->vertices[i5 + 13] = sin(i * anglePs - thisCraterAngle * 0.5) * (thisCraterSize + craterSize) / 2;
        entity->vertices[i5 + 14] = 0;

        unsigned char index = i % sides;
        thisCraterAngle = craterAngle * craterRatio[index];
        thisCraterSize = size * ((craterRatio[index] - 0.5) * 0.1 + 1);
        antiCraterSize = craterSize * 1.5 - thisCraterSize * 0.5;

        entity->vertices[i5 + 15] = 0;
        entity->vertices[i5 + 16] = 0;
        entity->vertices[i5 + 17] = cos(i * anglePs + thisCraterAngle * 0.5) * (thisCraterSize + craterSize) / 2;
        entity->vertices[i5 + 18] = sin(i * anglePs + thisCraterAngle * 0.5) * (thisCraterSize + craterSize) / 2;
        entity->vertices[i5 + 19] = 0;

        entity->vertices[i5 + 20] = 0;
        entity->vertices[i5 + 21] = 0;
        entity->vertices[i5 + 22] = cos(i * anglePs + thisCraterAngle) * thisCraterSize;
        entity->vertices[i5 + 23] = sin(i * anglePs + thisCraterAngle) * thisCraterSize;
        entity->vertices[i5 + 24] = 0;

        entity->vertices[i5 + 25] = 0;
        entity->vertices[i5 + 26] = 0;
        entity->vertices[i5 + 27] = cos(i * anglePs + thisCraterAngle * 0.5) * antiCraterSize;
        entity->vertices[i5 + 28] = sin(i * anglePs + thisCraterAngle * 0.5) * antiCraterSize;
        entity->vertices[i5 + 29] = 0;

        unsigned int i3 = i * 12;
        unsigned int i6 = i * 6;

        entity->indices[i3] = i6;
        entity->indices[i3 + 1] = i6 + 1;
        entity->indices[i3 + 2] = i6 + 2;

        entity->indices[i3 + 3] = i6;
        entity->indices[i3 + 4] = i6 + 2;
        entity->indices[i3 + 5] = i6 + 3;

        entity->indices[i3 + 6] = i6;
        entity->indices[i3 + 7] = i6 + 3;
        entity->indices[i3 + 8] = i6 + 4;

        entity->indices[i3 + 9] = i6;
        entity->indices[i3 + 10] = i6 + 4;
        entity->indices[i3 + 11] = i6 + 5;
    }

    float anglePs2 = 6.2831853 / asteroidSize;

    for (unsigned char i = 0; i < craterNum; i++) {
        float thisPos = craterSize * (cos(i * 0.5) * 0.4 + 0.2 + rand() % 10 * 0.01);
        float thisCraterSize = craterSize * (0.1 + rand() % 10 * 0.01);
        float x = cos((i + rand() % 10 * 0.05) * anglePs2) * thisPos;
        float y = sin((i + rand() % 10 * 0.05) * anglePs2) * thisPos;
        unsigned int pos = sides * 30 + i * (craterResolution * 5);

        for (unsigned char j = 0; j < craterResolution; j++) {
            float angle = 6.2831853 * (j - 0.5 + rand() % 10 * 0.1) / craterResolution;
            entity->vertices[pos + j * 5] = 0;
            entity->vertices[pos + j * 5 + 1] = 0;
            entity->vertices[pos + j * 5 + 2] = x + cos(angle) * thisCraterSize;
            entity->vertices[pos + j * 5 + 3] = y + sin(angle) * thisCraterSize;
            entity->vertices[pos + j * 5 + 4] = 0;
        }

        pos = sides * 6 + i * craterResolution * 3;
        unsigned int i3 = sides * 12 + i * (craterResolution - 2) * 3;
        for (unsigned char j = 0; j < craterResolution - 2; j++) {
            entity->indices[i3 + j * 3] = pos;
            entity->indices[i3 + j * 3 + 1] = pos + j + 1;
            entity->indices[i3 + j * 3 + 2] = pos + j + 2;
        }
    }

    $g(Entity, init)(entity);

    asteroid->staticEntities[1] = entity;

    free(craterRatio);

    entity = new($$g(Entity));
    RGBA(entity->color, 1, 0, 0, 1);

    entity->vertices = new_array(float, sides * 5);
    entity->vertices_size = sides;
    entity->indices = new_array(int, sides * 2);
    entity->indices_size = sides * 2;

    for (unsigned char i = 0; i < sides; i++) {
        unsigned int i5 = i * 5;
        entity->vertices[i5] = 0;
        entity->vertices[i5 + 1] = 0;
        entity->vertices[i5 + 2] = cos((i + 0.5) * anglePs) * size * 1.05;
        entity->vertices[i5 + 3] = sin((i + 0.5) * anglePs) * size * 1.05;
        entity->vertices[i5 + 4] = 0;

        entity->indices[i * 2] = i;
        entity->indices[i * 2 + 1] = (i + 1) % sides;
    }

    $g(Entity, init)(entity);
    entity->stroke = 1;

    asteroid->staticEntities[2] = entity;

    return 0;
}



void $c(Inline, onAsteroidsAsteroidCreate)(int argc, void **argv)
{
    if (argc < 1) {
        return;
    }
    unsigned char asteroidSize =
        (argc < 2 || *(unsigned char*)argv[1] < 1 || *(unsigned char*)argv[1] > 5)
            ? (unsigned char)(6 - pow(1 - rand() % 25 * 0.04, 0.5) * 5)
            : *(unsigned char*)argv[1];
    $$c(Object) *asteroid = argv[0];
    asteroid->argc = 5;
    asteroid->argv = new_array(void*, 5);
    $aeach(void*, arg, asteroid->argv, 0, 4) {
        *arg = new(float);
        *(float*)*arg = 0;
    }
    asteroid->argv[4] = new(unsigned char);

    *(float*)asteroid->argv[ASTEROID_ARG_X] = 142.22;
    *(float*)asteroid->argv[ASTEROID_ARG_Y] = rand() % 80 - 40;
    *(float*)asteroid->argv[ASTEROID_ARG_SX] = -pow(6 - asteroidSize, 2) * 0.1 * (rand() % 5 + 5);
    *(float*)asteroid->argv[ASTEROID_ARG_SY] = rand() % 10 * 0.01 - 0.05;
    *(unsigned char*)asteroid->argv[ASTEROID_ARG_SIZE] = asteroidSize;

    alloc_array(void*, nargv, 2);
    nargv[0] = argv[0];
    nargv[1] = new(unsigned char);
    *(unsigned char*)nargv[1] = asteroidSize;

    $g(Call, onRender)($g(Inline, onAsteroidsAsteroidCreate), 2, nargv);

    if (argc > 1) {
        free(argv[1]);
    }
}



void $p(Inline, processAsteroidMovement)($$c(Object) *asteroid)
{
    *(float*)asteroid->argv[ASTEROID_ARG_X] += *(float*)asteroid->argv[ASTEROID_ARG_SX] * 0.1;
    *(float*)asteroid->argv[ASTEROID_ARG_Y] += *(float*)asteroid->argv[ASTEROID_ARG_SY] * 0.1;
}


void $p(Inline, onAsteroidsAsteroidProcess)(int argc, void **argv)
{
    if (argc < 1) {
        return;
    }
    $$c(Object) *asteroid = argv[0];
    $p(Inline, processAsteroidMovement)(asteroid);
    if (*(float*)asteroid->argv[ASTEROID_ARG_X] < -142.22) {
        asteroid->$$_p(active) = 0;
    }
}



void $g(Inline, onAsteroidsAsteroidRender)(int argc, void **argv)
{
    if (argc < 3 || !argv[0] || !argv[1] || !argv[2]) {
        return;
    }
    $$c(Object) *asteroid = argv[0];
    if (asteroid->staticEntitiesNum < 2) {
        return;
    }

    asteroid->staticEntities[2]->translate[0] = asteroid->staticEntities[1]->translate[0] = asteroid->staticEntities[0]->translate[0] = *(float*)asteroid->argv[ASTEROID_ARG_X];
    asteroid->staticEntities[2]->translate[1] = asteroid->staticEntities[1]->translate[1] = asteroid->staticEntities[0]->translate[1] = *(float*)asteroid->argv[ASTEROID_ARG_Y];

    asteroid->staticEntities[2]->rotate[2] = asteroid->staticEntities[1]->rotate[2] = asteroid->staticEntities[0]->rotate[2] = 1;
    asteroid->staticEntities[2]->rotate[3] = asteroid->staticEntities[1]->rotate[3] = asteroid->staticEntities[0]->rotate[3] -= *(float*)asteroid->argv[ASTEROID_ARG_SY] * 0.05 * (6 - *(unsigned char*)asteroid->argv[ASTEROID_ARG_SIZE]);

    $g(Entity, render)(asteroid->staticEntities[0], $g(Shader, get)("simple"), NULL, argv[1], argv[2]);
    $g(Entity, render)(asteroid->staticEntities[1], $g(Shader, get)("simple"), NULL, argv[1], argv[2]);
    //$g(Entity, render)(asteroid->staticEntities[2], $g(Shader, get)("simple"), NULL, argv[1], argv[2]);
}

void $c(ObjectInitializer, initAsteroidsAsteroid)()
{
    $$c(List)* eventHandlerGroups = $c(List, create)();
    $c(EventHandler, register)(
        eventHandlerGroups,
        RHIC_OBJECT_EVENT_TYPE_CREATE,
        (void*)$c(Inline, onAsteroidsAsteroidCreate),
        0,
        NULL
    );
    $c(EventHandler, register)(
        eventHandlerGroups,
        RHIC_OBJECT_EVENT_TYPE_PROCESS,
        (void*)$p(Inline, onAsteroidsAsteroidProcess),
        0,
        NULL
    );
    $c(EventHandler, register)(
        eventHandlerGroups,
        RHIC_OBJECT_EVENT_TYPE_RENDER,
        (void*)$g(Inline, onAsteroidsAsteroidRender),
        0,
        NULL
    );

    $c(Object, registerEntry)("asteroid", eventHandlerGroups);
}
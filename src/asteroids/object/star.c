
#include "../../rhic/gl/texture.h"
#include "../../rhic/gl/shader.h"
#include "../../rhic/gl/entity.h"
#include "../../rhic/common/list.h"
#include "../../rhic/common/color.h"
#include "../../rhic/common/context.h"
#include "../../rhic/common/object.h"
#include "../../rhic/common/eventhandler.h"
#include "../../rhic/common/pointer.h"
#include "../init.h"

#include "star.h"

void *$g(Inline, onAsteroidsStarCreate)(int argc, void **argv)
{
    if (argc < 1) {
        return 0;
    }
    $$c(Object) *star = argv[0];
    star->staticEntitiesNum = 1;
    star->staticEntities = new_array($$g(Entity), 1);

    float vertices[] = {
            0, 0, -0.25, -0.25, 0,
            0, 1, -0.25, 0.25, 0,
            1, 0, 0.25, -0.25, 0,
            1, 1, 0.25, 0.25, 0,
    };

    int indices[] = {
            0, 1, 2, 1, 2, 3
    };

    $$g(Entity) *entity = new($$g(Entity));
    RGBA(entity->color, 1, 1, 1, 1);
    entity->vertices = new_array(float, 20);
    entity->vertices_size = 4;
    copy(vertices, entity->vertices);
    entity->indices = new_array(int, 6);
    entity->indices_size = 6;
    copy(indices, entity->indices);

    $g(Entity, init)(entity);

    star->staticEntities[0] = entity;

    return 0;
}


void $c(Inline, onAsteroidsStarCreate)(int argc, void **argv)
{
    if (argc < 1) {
        return;
    }
    $$c(Context) *context = $c(ContextSet, getContext)($_c(Asteroids, contextSet), "default");
    $$c(Object) *star = argv[0];
    star->argc = 3;
    star->argv = new_array(float*, 3);
    star->argv[0] = new(float);
    *(float*)(star->argv[0]) = (float)(rand() % 1422 - 711);
    star->argv[1] = new(float);
    *(float*)(star->argv[1]) = (float)(rand() % 800 - 400);
    star->argv[2] = new(float);
    *(float*)(star->argv[2]) = (float)(rand() % 30 + 20) * 0.1;

    void **nargv = new(void*);
    *nargv = *argv;

    $g(Call, onRender)($g(Inline, onAsteroidsStarCreate), 1, nargv);
}


void $g(Inline, onAsteroidsStarRender)(int argc, void **argv)
{
    if (argc < 3 || !argv[1] || !argv[2]) {
        return;
    }
    $$c(Object) *star = argv[0];
    if (star->staticEntitiesNum < 1) {
        return;
    }
    $$c(Context) *context = $c(ContextSet, getContext)($_c(Asteroids, contextSet), "default");
    float *posX = star->argv[0];
    float *posY = star->argv[1];
    float *speedX = star->argv[2];
    *posX = *posX + *speedX;
    if (*posX > 711) {
        *posX -= 1422;
        *posY = (float)(rand() % 800 - 400);
        *speedX = (float)(rand() % 30 + 20) * 0.1;
    }
    star->staticEntities[0]->translate[0] = -*posX * 0.1;
    star->staticEntities[0]->translate[1] = *posY * 0.1;

    $g(Entity, render)(star->staticEntities[0], $g(Shader, get)("simple"), NULL, argv[1], argv[2]);
}


void $c(ObjectInitializer, initAsteroidsStar)()
{
    $$c(List)* eventHandlerGroups = $c(List, create)();
    $c(EventHandler, register)(
        eventHandlerGroups,
        RHIC_OBJECT_EVENT_TYPE_CREATE,
        (void*)$c(Inline, onAsteroidsStarCreate),
        0,
        NULL
    );
    $c(EventHandler, register)(
        eventHandlerGroups,
        RHIC_OBJECT_EVENT_TYPE_RENDER,
        (void*)$g(Inline, onAsteroidsStarRender),
        0,
        NULL
    );

    $c(Object, registerEntry)("star", eventHandlerGroups);
}

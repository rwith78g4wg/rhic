/**
 * This header file defines callback functions.
 *
 * When the project will grow enough, their implementation may will be separated by numerous source files.
 */
#ifndef ASTEROIDS_CALLBACK_H
#define ASTEROIDS_CALLBACK_H

#include "../rhic/common/macro.h"

$$c(Context);
$$c(Pointer);


/**
 * Initializes process thread, sets up scene and creates triangle object
 *
 * @param context
 * @return Error code
 */

int $p(Asteroids, onProcessInit)($$c(Context) *window);


/**
 * Frees the scene and the triangle initializer
 *
 * @param window
 * @return Error code
 */

int $p(Asteroids, onProcessFree)($$c(Context) *window);


/**
 * Process thread loop function
 *
 * @param window
 * @return If equals -1 continues the loop, else stops the execution with the given error code.
 */

int $p(Asteroids, onProcess)($$c(Context) *window);


/**
 * Render thread loop function
 *
 * @param window
 * @return If equals -1 continues the loop, else stops the execution with the given error code.
 */

int $g(Asteroids, onRender)($$c(Context) *window);


/**
 * KeyDown event handler
 *
 * @param key
 */

void $p(Asteroids, onKeyDown)(unsigned char key);


/**
 * KeyUp event handler
 *
 * @param key
 */

void $p(Asteroids, onKeyUp)(unsigned char key);


/**
 * Context resize event handler. Fires context resize event inside render thread
 *
 * @param width New width of the context
 * @param height New height of the context
 */

void $p(Asteroids, onResize)($_t(uhuge) width, $_t(uhuge) height);


/**
 * PointerDown event handler
 *
 * @param pointer Pointer being updated
 */

void $p(Asteroids, onPointerDown)($$c(Pointer) *pointer);


/**
 * PointerUp event handler
 *
 * @param pointer Pointer being updated
 */

void $p(Asteroids, onPointerUp)($$c(Pointer) *pointer);

#endif //ASTEROIDS_CALLBACK_H

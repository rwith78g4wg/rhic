#include <stdio.h>
#include <math.h>

#include "config.h"
#include "init.h"

#include "../rhic/gl/renderer.h"
#include "../rhic/common/list.h"
#include "../rhic/common/context.h"
#include "../rhic/common/pointer.h"
#include "../rhic/common/object.h"
#include "../rhic/common/eventhandler.h"
#include "../rhic/common/scene.h"

#include "callback.h"


/**
 * Setups orthographic projection matrix
 *
 * @param matrix Matrix to be modified
 * @param left Left border of the context
 * @param top Top border of the context
 * @param right Right border of the context
 * @param bottom Bottom border of the context
 * @param near Nearest depth of the view to be displayed
 * @param far Farthest depth of the view to be displayed
 */

void $c(Inline, setOrtho)(float matrix[16], float left, float top, float right, float bottom, float near, float far)
{
    matrix[0] = 2 / (right - left);
    matrix[1] = 0;
    matrix[2] = 0;
    matrix[3] = 0;
    matrix[4] = 0;
    matrix[5] = 2 / (top - bottom);
    matrix[6] = 0;
    matrix[7] = 0;
    matrix[8] = 0;
    matrix[9] = 0;
    matrix[10] = 2 / (near - far);
    matrix[11] = 0;
    matrix[12] = (left + right) / (left - right);
    matrix[13] = (bottom + top) / (bottom - top);
    matrix[14] = (near + far) / (near - far);
    matrix[15] = 1;
}


/**
 * Setups frustum projection matrix
 *
 * @param matrix Matrix to be modified
 * @param fov Field of view
 * @param aspect Aspect of context dimensions
 * @param zNear Nearest depth of the view to be displayed
 * @param zFar Farthest depth of the view to be displayed
 */

void $c(Inline, setFrustum)(float matrix[16], float fov, float aspect, float zNear, float zFar)
{
    float max = zNear * tan(fov) / aspect;

    float depth = zFar - zNear;
    float q = -(zFar + zNear) / depth;
    float qn = -2 * zFar * zNear / depth;

    float size = zNear / max;

    matrix[0] = size / aspect;
    matrix[1] = 0;
    matrix[2] = 0;
    matrix[3] = 0;

    matrix[4] = 0;
    matrix[5] = size;
    matrix[6] = 0;
    matrix[7] = 0;

    matrix[8] = 0;
    matrix[9] = 0;
    matrix[10] = q;
    matrix[11] = -1;

    matrix[12] = 0;
    matrix[13] = 0;
    matrix[14] = qn;
    matrix[15] = 0;
}


/**
 * Changes scene projection matrix. The handler is called on context initialization and resize.
 *
 * @param argc
 * @param argv
 * @return
 */

void *$c(Inline, setSceneProjection)(int argc, void **argv)
{
    if (argc < 1) {
        return NULL;
    }

    $$c(SceneSet) **set = argv[0];
    $$c(Context) *context = $c(ContextSet, getContext)($_c(Asteroids, contextSet), "default");

    float aspect = (float)context->height / context->width;

    if (aspect > ASTEROIDS_ASPECT_RATIO) {
        float zoom = 40 / ASTEROIDS_ASPECT_RATIO;
        $c(Inline, setOrtho)((*set)->projectionMatrix, -zoom, -zoom * aspect, zoom, zoom * aspect, 0.00001, 10000);
    } else {
        $c(Inline, setOrtho)((*set)->projectionMatrix, -40 / aspect, -40, 40 / aspect, 40, 0.00001, 10000);
    }

    return NULL;
}


int $p(Asteroids, onProcessInit)($$c(Context) *context)
{
    $_c(Asteroids, contextSet) = $c(List, create)();
    $c(ContextSet, addContext)($_c(Asteroids, contextSet), "default", context);
    $c(EventHandler, register)(
        context->sceneSet->eventHandlerGroups,
        RHIC_SCENE_EVENT_TYPE_REFRESH_PROJECTION,
        $c(Inline, setSceneProjection),
        1,
        (void**)&context->sceneSet
    );

    $c(Asteroids, initObjectEntries)();
    $c(Asteroids, initScenes)(context);

    return 0;
}


int $p(Asteroids, onProcess)($$c(Context) *context)
{
    $p(SceneSet, process)(context->sceneSet);

    if (rand() % 75 == 0) {
        $$c(Object) *asteroid = $c(Object, create)("asteroid", 0, NULL);

        $p(Scene, addObject)($c(SceneSet, getScene)(context->sceneSet, "asteroids"), asteroid);
    }
    return -1;
}


int $p(Asteroids, onProcessFree)($$c(Context) *context)
{
    $c(ContextSet, free)($_c(Asteroids, contextSet));
    return 0;
}


int $g(Asteroids, onRender)($$c(Context) *context)
{
    $g(SceneSet, render)(context->sceneSet);
    $g(PlatformSpecific, render)(context);
    return -1;
}


/**
 * Context resize event handler in render thread. Updates viewport of context and scene projection matrix
 *
 * @param argc
 * @param argv
 * @return
 */

void *$g(Inline, onResize)(int argc, void **argv)
{
    $_t(uhuge) width = *($_t(uhuge)*)argv[0];
    $_t(uhuge) height = *($_t(uhuge)*)argv[1];

    free(argv[0]);
    free(argv[1]);

    $$c(Context) *context = $c(ContextSet, getContext)($_c(Asteroids, contextSet), "default");
    glViewport(0, 0, width, height);
    $c(SceneSet, fireSceneEvent)(&context->sceneSet, RHIC_SCENE_EVENT_TYPE_REFRESH_PROJECTION, 0, NULL);
    return NULL;
}


void $p(Asteroids, onResize)($_t(uhuge) width, $_t(uhuge) height)
{
    alloc_array(void*, argv, 2);
    argv[0] = new($_t(uhuge));
    *($_t(uhuge)*)argv[0] = width;
    argv[1] = new($_t(uhuge));
    *($_t(uhuge)*)argv[1] = height;
    $g(Call, onRender)($g(Inline, onResize), 2, argv);
}


void $p(Asteroids, onPointerDown)($$c(Pointer) *pointer)
{
    $c(SceneSet, fireObjectEvent)($c(ContextSet, getContext)($_c(Asteroids, contextSet), "default")->sceneSet, RHIC_OBJECT_EVENT_TYPE_POINTER_DOWN, 1, (void**)&pointer);
}


void $p(Asteroids, onPointerUp)($$c(Pointer) *pointer)
{
    $c(SceneSet, fireObjectEvent)($c(ContextSet, getContext)($_c(Asteroids, contextSet), "default")->sceneSet, RHIC_OBJECT_EVENT_TYPE_POINTER_UP, 1, (void**)&pointer);
}

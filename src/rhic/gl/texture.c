#include <stdio.h>
#include <math.h>

#include "../common/list.h"
#include "../common/storage.h"
#include "../common/rhg.h"

#include "texture.h"

void $g(Texture, init)($$g(Texture) *texture)
{
    glGenTextures(1, &(texture->texture));
    glBindTexture(GL_TEXTURE_2D, texture->texture);
    glTexImage2D(
        GL_TEXTURE_2D,
        0,
        GL_RGBA,
        texture->width,
        texture->size / texture->width,
        0,
        GL_RGBA,
        GL_UNSIGNED_BYTE, texture->data);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
}

void *$g(Texture, getFromRhg)(char *name, $_t(uhuge) index)
{
    unsigned char sIndexSize = log10(SIZE_MAX) + 2;
    alloc_array(char, sIndex, sIndexSize);
    sprintf(sIndex, "%zu", index);
    alloc_array(char, key, strlen(name) + sIndexSize + 5);
    strcpy(key, "RHG#");
    strcat(key, sIndex);
    strcat(key, "#");
    strcat(key, name);

    free(sIndex);

    $$c(Item) *item = $c(Item, get)($_c(Storage, texture), key);

    if (item) {
        free(key);
        return item->value;
    }
    $$g(Rhg) *obRhg = $g(Rhg, get)(name);
    $$g(Texture) *obTexture = new($$g(Texture));
    obTexture->data = $g(Rhg, apply)(obRhg, index);
    obTexture->size = (GLuint)obRhg->data_size;
    obTexture->width = (GLuint)obRhg->width;
    $g(Texture, init)(obTexture);
    $c(List, push)($_c(Storage, texture), $c(Item, create)(key, obTexture));
    free(key);
    return obTexture;
}

void $g(Texture, free)($$g(Texture) *texture)
{
    glDeleteTextures(1, &texture->texture);
    free(texture->data);
    free(texture);
}
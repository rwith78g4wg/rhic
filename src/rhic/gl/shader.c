#include "all/renderer.h"
#include "../common/file.h"
#include "../common/list.h"
#include "../common/storage.h"

#include "shader.h"

$$g(Shader)* $g(Shader, get)(char *name)
{
    $$c(Item) *item = $c(Item, get)($_c(Storage, shader), name);
    if (item) {
        return item->value;
    }
    unsigned int size = strlen(name) + 15 + strlen(RHIC_DATA_PATH);
    alloc_array(char, fileName, size);
    strcpy(fileName, RHIC_DATA_PATH "shaders/");
    strcat(fileName, name);
    strcat(fileName, "_v.glsl");
    fget(shaderVertex, fileName);
    fileName[size - 6] = 'f';
    fget(shaderFragment, fileName);
    free(fileName);
    $$g(Shader) *obShader = $g(Shader, compile)(shaderVertex, shaderFragment);
    free(shaderFragment);
    free(shaderVertex);
    $c(List, push)($_c(Storage, shader), $c(Item, create)(name, obShader));
    return obShader;
}

$$g(Shader) *$g(Shader, compile)(char* sVertex, char* sFragment)
{
    if (sVertex == NULL || sFragment == NULL) {
        return NULL;
    }

    GLint compiled = GL_FALSE;

    GLuint nVertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(nVertex, 1, (const GLchar**)&sVertex, NULL);
    glCompileShader(nVertex);
    glGetShaderiv(nVertex, GL_COMPILE_STATUS, &compiled);

    if (!compiled) {
        GLint maxLength = 0;
        glGetShaderiv(nVertex, GL_INFO_LOG_LENGTH, &maxLength);

        char *log = new_array(char, maxLength);
        glGetShaderInfoLog(nVertex, maxLength, NULL, log);
        fprintf(stderr, "Error: %s", log);
        glDeleteShader(nVertex);
        return NULL;
    }

    GLuint nFragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(nFragment, 1, (const GLchar**)&sFragment, NULL);
    glCompileShader(nFragment);
    glGetShaderiv(nFragment, GL_COMPILE_STATUS, &compiled);

    if (!compiled) {
        GLint maxLength = 0;
        glGetShaderiv(nFragment, GL_INFO_LOG_LENGTH, &maxLength);

        char *log = new_array(char, maxLength);
        glGetShaderInfoLog(nFragment, maxLength, NULL, log);
        fprintf(stderr, "Error: %s", log);
        glDeleteShader(nFragment);
        return NULL;
    }

    GLuint nProgram = glCreateProgram();
    glAttachShader(nProgram, nVertex);
    glAttachShader(nProgram, nFragment);
    glLinkProgram(nProgram);

    glGetProgramiv(nProgram, GL_LINK_STATUS, &compiled);

    glDeleteShader(nVertex);
    glDeleteShader(nFragment);

    alloc($$g(Shader), shader);
    shader->program = nProgram;

    shader->atPosition = glGetAttribLocation(shader->program, "at_Position");
    shader->atTexCoord = glGetAttribLocation(shader->program, "at_TexCoord");

    shader->uColor = glGetUniformLocation(shader->program, "uColor");
    shader->uTexture0 = glGetUniformLocation(shader->program, "uTexture0");

    shader->uRotate = glGetUniformLocation(shader->program, "uRotate");
    shader->uTranslate = glGetUniformLocation(shader->program, "uTranslate");
    shader->uScale = glGetUniformLocation(shader->program, "uScale");

    shader->uProjectionMatrix = glGetUniformLocation(shader->program, "uProjectionMatrix");
    shader->uViewMatrix = glGetUniformLocation(shader->program, "uViewMatrix");

    return shader;
}

void $g(Shader, free)($$g(Shader) *shader)
{
    if (shader != NULL) {
        glDeleteProgram(shader->program);
    }
    free(shader);
}
#ifndef RHIC_GL_SHADER_H
#define RHIC_GL_SHADER_H

#include "GL/gl.h"

#include "../common/macro.h"

$$c(List);

$$g(Shader) {
    GLuint program;

    GLint uColor;
    GLint uTexture0;
    GLint uRotate, uTranslate, uScale;
    GLint uProjectionMatrix, uViewMatrix;

    GLint atPosition;
    GLint atTexCoord;
};

$$c(List) *$_c(Storage, shader);


/**
 * Compiles GLSL shader from given vertex and fragment source code
 * @param vertex
 * @param fragment
 * @return
 */

$$g(Shader) *$g(Shader, compile)(char* vertex, char* fragment);


/**
 * Frees GLSL shader and shader object
 * @param shader
 */

void $g(Shader, free)($$g(Shader) *shader);


/**
 * Gets shader object by its name
 * @param name
 * @return
 */

$$g(Shader)* $g(Shader, get)(char *name);

#endif //RHIC_GL_SHADER_H

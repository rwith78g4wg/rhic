#ifndef RHIC_GL_ENTITY_H
#define RHIC_GL_ENTITY_H

#include <GL/glew.h>

#include "shader.h"
#include "texture.h"

$$g(Entity)
{
    GLuint index_buffer, vertex_buffer;

    GLfloat color[4];
    GLfloat *translate,
            *rotate,
            *scale;
    GLfloat *vertices;
    GLuint vertices_size;
    GLint *indices;
    GLuint indices_size;
    _Bool stroke;
};


/**
 * Initializes GL buffers of indices and vertices of the entity
 * @param entity
 */

void $g(Entity, init)($$g(Entity) *entity);


/**
 * Renders an entity
 * @param entity Entity to render
 * @param shader Shader to be render with
 * @param texture Texture to render entity with
 * @param projectionMatrix Projection matrix of the scene
 * @param viewMatrix View matrix of the scene
 * @return Error code
 */

int $g(Entity, render)($$g(Entity) *entity, $$g(Shader) *shader, $$g(Texture) *texture, float *projectionMatrix, float *viewMatrix);


/**
 * Updates entity vertices buffer according to its array
 * @param entity
 */

void $g(Entity, update)($$g(Entity) *entity);

/**
 * Frees entity and its buffers
 * @param entity
 */

void $g(Entity, free)($$g(Entity) *entity);

#endif //RHIC_GL_ENTITY_H

#ifndef RHIC_GL_ALL_RENDERER_H
#define RHIC_GL_ALL_RENDERER_H

#include <GL/glew.h>

#include "../../common/macro.h"

$$c(Context);

int $(Graphics, Core, init)($$c(Context) *window);
int $(Graphics, Core, free)($$c(Context) *window);

#endif //RHIC_GL_ALL_RENDERER_H

#include "all/renderer.h"

#include "entity.h"

void $g(Entity, init)($$g(Entity) *entity)
{
    entity->stroke = 0;
    entity->translate = new_array(float, 4);
    entity->translate[0] = entity->translate[1] = entity->translate[2] = 0;
    entity->translate[3] = 1;
    entity->rotate = new_array(float, 4);
    entity->rotate[0] = entity->rotate[1] = entity->rotate[2] = 0;
    entity->rotate[3] = 6.28318531;
    entity->scale = new_array(float, 4);
    entity->scale[0] = entity->scale[1] = entity->scale[2] = entity->scale[3] = 1;

    {
        glGenBuffers(1, &(entity->index_buffer));
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (entity->index_buffer));

        glBufferData(GL_ELEMENT_ARRAY_BUFFER, entity->indices_size * sizeof(typeof(*(entity->indices))), entity->indices, GL_STATIC_DRAW);

        GLint size = 0;
        glGetBufferParameteriv(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
        if (entity->indices_size * sizeof(typeof(*(entity->indices))) != size) {
            $g(Entity, free)(entity);
            return;
        }
    }

    {
        glGenBuffers(1, &(entity->vertex_buffer));
        glBindBuffer(GL_ARRAY_BUFFER, (entity->vertex_buffer));
        GLint actual_size = entity->vertices_size * sizeof(GLfloat) * 5;
        glBufferData(GL_ARRAY_BUFFER, actual_size, entity->vertices, GL_STATIC_DRAW);

        GLint size = 0;
        glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
        if (actual_size != size) {
            $g(Entity, free)(entity);
            return;
        }
    }
}

void $g(Entity, update)($$g(Entity) *entity)
{
    glBindBuffer(GL_ARRAY_BUFFER, (entity->vertex_buffer));
    glBufferData(GL_ARRAY_BUFFER, entity->vertices_size * sizeof(GLfloat) * 3, entity->vertices, GL_STATIC_DRAW);
}

int $g(Entity, render)($$g(Entity) *entity, $$g(Shader) *shader, $$g(Texture) *texture, float *projectionMatrix, float *viewMatrix)
{
    glActiveTexture(GL_TEXTURE0);
    if (texture) {
        glBindTexture(GL_TEXTURE_2D, texture->texture);
    } else {
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    glUseProgram(shader->program);

    glUniform1i(shader->uTexture0, 0);
    glUniform4f(
        shader->uColor,
        entity->color[0],
        entity->color[1],
        entity->color[2],
        texture ? entity->color[3] : -entity->color[3]
    );

    glUniform4fv(shader->uRotate, 1, entity->rotate);
    glUniform4fv(shader->uTranslate, 1, entity->translate);
    glUniform4fv(shader->uScale, 1, entity->scale);

    if (projectionMatrix) {
        glUniformMatrix4fv(shader->uProjectionMatrix, 1, GL_FALSE, projectionMatrix);
    }
    if (viewMatrix) {
        glUniformMatrix4fv(shader->uViewMatrix, 1, GL_FALSE, viewMatrix);
    }

    glBindBuffer(GL_ARRAY_BUFFER, entity->vertex_buffer);

    glEnableVertexAttribArray(shader->atTexCoord);
    glVertexAttribPointer(shader->atTexCoord, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), 0);

    glEnableVertexAttribArray(shader->atPosition);
    glVertexAttribPointer(shader->atPosition, 3, GL_FLOAT, GL_TRUE, 5 * sizeof(GLfloat), (void*)(2 * sizeof(GLfloat)));

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, entity->index_buffer);
    glDrawElements(entity->stroke ? GL_LINES : GL_TRIANGLES, entity->indices_size, GL_UNSIGNED_INT, NULL);

    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);

    return 0;
}

void $g(Entity, free)($$g(Entity) *entity) {
    glDeleteBuffers(1, &(entity->index_buffer));
    if (entity->indices != NULL) {
        free(entity->indices);
        entity->indices = NULL;
    }

    glDeleteBuffers(1, &(entity->vertex_buffer));
    if (entity->vertices != NULL) {
        free(entity->vertices);
        entity->vertices = NULL;
    }

    free(entity->scale);
    free(entity->rotate);
    free(entity->translate);
    free(entity);
}
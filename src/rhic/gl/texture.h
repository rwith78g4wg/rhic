#ifndef RHIC_GL_TEXTURE_H
#define RHIC_GL_TEXTURE_H

#include <GL/glew.h>

#include "../common/macro.h"

$$c(List);

$$g(Texture)
{
    GLuint texture;

    GLubyte *data;
    GLuint width;
    GLuint size;
};

$$c(List) *$_c(Storage, texture);


/**
 * Initializes GL texture from data
 * @param texture
 */

void $g(Texture, init)($$g(Texture) *texture);


/**
 * Creates texture object from rhg of given palette index
 * @param name
 * @param index
 * @return
 */

void *$g(Texture, getFromRhg)(char *name, $_t(uhuge) index);


/**
 * Frees GL texture and texture object
 * @param texture
 */

void $g(Texture, free)($$g(Texture) *texture);

#endif //RHIC_GL_TEXTURE_H

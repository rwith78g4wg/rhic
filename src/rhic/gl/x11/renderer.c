#ifdef __unix__
#include "GL/glew.h"
#include "GL/glxew.h"

#include "../../common/x11/context.h"
#include "../all/renderer.h"

#include "renderer.h"

XVisualInfo *vi;

int $g(PlatformSpecific, init)($$c(Context) *window)
{
    glxewInit();

    GLint att[] = {
        GLX_RGBA,
        GLX_DEPTH_SIZE, 16,
        GLX_DOUBLEBUFFER,
        None
    };

    vi = glXChooseVisual(window->display, window->screen, att);

    if (vi == 0) {
        return 1;
    }

    if (!(window->attr[0] = (void*)glXCreateContext(window->display, vi, NULL, GL_TRUE))) {
        XFree(vi);
        return 2;
    }

    if (!glXMakeCurrent(window->display, window->window, window->attr[0])) {
        XFree(vi);
        glXDestroyContext(window->display, window->attr[0]);
        return 3;
    }

    if (glewInit() || !GLEW_VERSION_2_0) {
        XFree(vi);
        glXMakeCurrent(window->display, 0, NULL);
        glXDestroyContext(window->display, window->attr[0]);
        return 4;
    }

    if (glewIsExtensionSupported("GLX_EXT_ap_control")) {
        glXSwapIntervalEXT(window->display, window->window, 1);
    } else {
        if (glewIsExtensionSupported("GLX_MESA_ap_control")) {
            glXSwapIntervalMESA(1);
        } else {
            if (glewIsExtensionSupported("GLX_SGI_ap_control")) {
                glXSwapIntervalSGI(1);
            }
        }
    }

    return $(Graphics, Core, init)(window);
}

int $g(PlatformSpecific, free)($$c(Context) *window)
{
    int status = $(Graphics, Core, free)(window);

    XFree(vi);
    glXMakeCurrent(window->display, 0, NULL);
    glXDestroyContext(window->display, (void*)window->attr[0]);

    return status;
}

void $g(PlatformSpecific, render)($$c(Context) *window)
{
    glXSwapBuffers(window->display, window->window);
}
#endif //__unix__
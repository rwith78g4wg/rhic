#ifdef __unix__
#ifndef RHIC_GL_X11_RENDERER_H
#define RHIC_GL_X11_RENDERER_H

#include "../../common/macro.h"

$$c(Context);

int $g(PlatformSpecific, free) ($$c(Context) *window);
int $g(PlatformSpecific, init) ($$c(Context) *window);
void $g(PlatformSpecific, render)($$c(Context) *window);

#endif //RHIC_GL_X11_RENDERER_H
#endif //__unix__

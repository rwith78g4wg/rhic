#ifndef RHIC_BASE_LIST_H
#define RHIC_BASE_LIST_H

#include "macro.h"
#include "list.h"

/**
 * List item object. Contains value of current item and pointers to previous and next items in a list.
 */

$$c(List)
{
    $$c(List) *next;
    $$c(List) *prev;
    void *value;
};

/**
 * List foreach with _index and _value additions.
 * _index contains current iteration index.
 * _value contains value of current item.
 *
 * @param type Type of item list
 * @param element Name of item variable. Additions are declared as variables with this name with an appropriate postfix
 * @param list List to be iterated
 */

#define $each_iv(type, element, list) \
$$c(List) *element##_iterator = list; \
type *element = (type*)(element##_iterator->value); \
type element##_value = *element; \
$_t(uhuge) element##_index = 0; \
for (; \
    element##_iterator->next; \
    (element##_iterator = ($$c(List)*)(element##_iterator->next)), \
    (element = (type*)(element##_iterator->value)), \
    (element##_value = *element), \
    (element##_index++) \
    )


/**
 * List foreach with an _index addition which contains current iteration index
 *
 * @param type Type of item list
 * @param element Name of item variable. _index addition is declared as variable with this name with an _index postfix
 * @param list List to be iterated
 */

#define $each_i(type, element, list) \
$$c(List) *element##_iterator = list; \
type *element = (type*)(element##_iterator->value); \
$_t(uhuge) element##_index = 0; \
for (; \
    element##_iterator->next; \
    (element##_iterator = ($$c(List)*)(element##_iterator->next)), \
    (element = (type*)(element##_iterator->value)), \
    (element##_index++) \
    )


/**
 * List foreach with an _value addition which contains value of current item
 *
 * @param type Type of item list
 * @param element Name of item variable. _value addition is declared as variable with this name with an _value postfix
 * @param list List to be iterated
 */

#define $each_v(type, element, list) \
$$c(List) *element##_iterator = list; \
type *element = (type*)(element##_iterator->value); \
type element##_value = *element; \
for (; \
    element##_iterator->next; \
    (element##_iterator = ($$c(List)*)(element##_iterator->next)), \
    (element = (type*)(element##_iterator->value)), \
    (element##_value = *element), \
    )


/**
 * List foreach
 *
 * @param type Type of item list
 * @param element Name of item variable
 * @param list List to be iterated
 */

#define $each(type, element, list) \
$$c(List) *element##_iterator = list; \
type *element = (type*)(element##_iterator->value); \
for (; \
    element##_iterator->next; \
    (element##_iterator = ($$c(List)*)(element##_iterator->next)), \
    (element = (type*)(element##_iterator->value)) \
    )


/**
 * Gets first element of the list
 * @param list
 * @return
 */

$$c(List) *$c(List, begin)($$c(List) *list);


/**
 * Gets next after last element of the list
 * @param list
 * @return
 */

$$c(List) *$c(List, end)($$c(List) *list);


/**
 * Gets element of the list at given index
 *
 * @param list
 * @param i
 * @return
 */

$$c(List) *$c(List, at)($$c(List) *list, int i);


/**
 * Gets element of the list at given index starting from the end
 *
 * @param list
 * @param i
 * @return
 */

$$c(List) *$c(List, rat)($$c(List) *list, int i);


/**
 * Counts number of elements before current
 *
 * @param list
 * @return
 */

$_t(uhuge) $c(List, left)($$c(List) *list);


/**
 * Counts number of elements after current
 *
 * @param list
 * @return
 */

$_t(uhuge) $c(List, right)($$c(List) *list);


/**
 * Counts number of elements in the list
 *
 * @param list
 * @return
 */

$_t(uhuge) $c(List, size)($$c(List) *list);


/**
 * Creates an empty list
 *
 * @return
 */

$$c(List) *$c(List, create)();


/**
 * Frees all items and their values in the list
 *
 * @param list
 */

void $c(List, free)($$c(List) *list);


/**
 * Frees all items and frees their values in the list leaving one empty item
 *
 * @param list
 */

void $c(List, clear)($$c(List) **list);


/**
 * Frees all items and their values before current element and returns value of the element before current
 *
 * @param list
 * @return
 */

void *$c(List, cutLeft)($$c(List) *list);


/**
 * Frees all items and their values after current element and the element itself
 * and returns value of the element
 *
 * @param list
 * @return
 */

void *$c(List, cutRight)($$c(List) **list);


/**
 * Frees last element of the list and returns its value
 *
 * @param list
 * @return
 */

void *$c(List, pop)($$c(List) **list);


/**
 * Frees first element of the list and returns its value
 *
 * @param list
 * @return
 */

void *$c(List, shift)($$c(List) **list);


/**
 * Frees all items and their values of current element and elements after
 *
 * @param list
 */

void $c(List, freeRight)($$c(List) **list);


/**
 * Frees all items and their values before current element
 *
 * @param list
 */

void $c(List, freeLeft)($$c(List) *list);


/**
 * Frees given element and returns its value
 *
 * @param list
 * @return
 */

void *$c(List, remove)($$c(List) **list, $$c(List) **item);


/**
 * Creates an element after current and sets its value
 *
 * @param list
 * @return
 */

$$c(List) *$c(List, setNext)($$c(List) *list, void *value);


/**
 * Creates an element before current and sets its value
 *
 * @param list
 * @return
 */

$$c(List) *$c(List, setPrev)($$c(List) *list, void *value);


/**
 * Creates an element at the end of the list and sets its value
 *
 * @param list
 * @return
 */

$$c(List) *$c(List, push)($$c(List) *list, void *value);


/**
 * Creates an element at the beginning of the list and sets its value
 *
 * @param list
 * @return
 */

$$c(List) *$c(List, unshift)($$c(List) *list, void *value);


/**
 * Creates a number of elements from an array, places them after current element and sets their values
 *
 * @param list
 * @return
 */

void *$c(List, insertAfter)($$c(List) *list, void *values, $_t(uhuge) num);


/**
 * Creates a number of elements from an array, places them before current element and sets their values
 *
 * @param list
 * @return
 */

void *$c(List, insertBefore)($$c(List) *list, void *values, $_t(uhuge) num);


/**
 * Creates a number of elements from an array, places them at the end of the list and sets their values
 *
 * @param list
 * @return
 */

void *$c(List, insertAsLast)($$c(List) *list, void *values, $_t(uhuge) num);


/**
 * Creates a number of elements from an array, places them at the beginning of the list and sets their values
 *
 * @param list
 * @return
 */

void *$c(List, insertAsFirst)($$c(List) *list, void *values, $_t(uhuge) num);

#endif //RHIC_BASE_LIST_H

#include <string.h>

#include "list.h"

#include "storage.h"

$$c(Item) *$c(Item, create)(char *key, void *value)
{
    alloc($$c(Item), item);
    item->key = new_array(char, strlen(key));
    strcpy(item->key, key);
    item->value = value;
    return item;
}

void *$c(Item, get)($$c(List) *storage, char *key)
{
    $each($$c(Item), item, storage) {
        if (!strcmp(item->key, key)) {
            return item;
        }
    }
    return NULL;
}

void *$c(Item, free)($$c(Item) *item)
{
    void *value = item->value;
    free(item->key);
    free(item);
    return value;
}

void $c(Storage, free)($$c(List) *storage)
{
    $each($$c(Item), item, storage) {
        $c(Item, free)(item);
    }
    $c(List, free)(storage);
}
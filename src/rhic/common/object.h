#ifndef RHIC_COMMON_OBJECT_H
#define RHIC_COMMON_OBJECT_H

#include "macro.h"

$$c(List);
$$c(Item);
$$g(Entity);

#define RHIC_OBJECT_EVENT_TYPE_CREATE 0
#define RHIC_OBJECT_EVENT_TYPE_PROCESS 1
#define RHIC_OBJECT_EVENT_TYPE_RENDER 2
#define RHIC_OBJECT_EVENT_TYPE_DESTROY 3
#define RHIC_OBJECT_EVENT_TYPE_POINTER_DOWN 4
#define RHIC_OBJECT_EVENT_TYPE_POINTER_UP 5


/**
 * Rhic object is a structure that contains and processes entities
 */

$$c(Object) {
    char *type;
    $_t(uhuge) staticEntitiesNum;
    void **argv;
    int argc;
    char $$_p(active), $$_g(active);
    $$g(Entity) **staticEntities;
    $$c(List) *dynamicEntities;
    $$c(List) *eventHandlerGroups;
};

$$c(List) *$_c(Object, entries);


/**
 * Fires an event related to the object
 * @param object
 * @param eventType
 * @param argc
 * @param argv
 */

void $c(Object, fireEvent)($$c(Object) **object, $_t(uhuge) eventType, int argc, void **argv);


/**
 * Registers new object entry
 * @param typeId Id of an entry
 * @param eventHandlerGroups Entry events
 * @return
 */

_Bool $c(Object, registerEntry)(char *type, $$c(List) *eventHandlerGroups);


/**
 * Searches for an entry by its id
 * @param typeId
 * @return List of event handler groups of an entry
 */

$$c(List) *$c(Object, findEntry)(char *type);


/**
 * Frees all entities
 */

void $c(Object, freeEntries)();


/**
 * Creates an object
 * @param typeId Id of an entry of the object
 * @param argc
 * @param argv
 * @return
 */

$$c(Object) *$c(Object, create)(char *type, int argc, void **argv);


/**
 * Frees an object
 * @param object
 */

void $c(Object, free)($$c(Object) *object);

#endif //RHIC_COMMON_OBJECT_H
#include <stdio.h>
#include <time.h>
#include <pthread.h>

#include "../context.h"
#include "../list.h"
#include "../storage.h"
#include "../eventhandler.h"
#include "../pointer.h"

#include "context.h"

const unsigned int
    frame_time = BILLION / (unsigned int)MAX_FRAME_RATE,
    proc_time  = BILLION / (unsigned int)MAX_PROC_RATE;

$_t(uhuge) $c(Time, now)()
{
    struct timespec spec;
    clock_gettime(CLOCK_REALTIME, &spec);

    return ($_t(uhuge)) spec.tv_sec * BILLION + ($_t(uhuge)) spec.tv_nsec;
}

void $c(Time, sleep)($_t(uhuge) nano)
{
    struct timespec spec;
    spec.tv_nsec = nano % BILLION;
    spec.tv_sec = nano / BILLION;
    nanosleep(&spec, NULL);
}

void *$p(Core, handleKeyDown)(int argc, void **argv)
{
    if (argc < 2) {
        return NULL;
    }
    $$c(Context) *context = argv[0];
    unsigned char ascKey = *(unsigned char *)(argv[1]);
    free(argv[1]);
    if (!context->keys[ascKey] && context->processor->key_down) {
        context->processor->key_down(ascKey);
    }
    context->keys[ascKey] = 1;
    free(argv);
    return NULL;
}

void *$p(Core, handleKeyUp)(int argc, void **argv)
{
    if (argc < 2) {
        return NULL;
    }
    $$c(Context) *context = argv[0];
    unsigned char ascKey = *(unsigned char *)(argv[1]);
    free(argv[1]);
    if (context->keys[ascKey] && context->processor->key_up) {
        context->processor->key_up(ascKey);
    }
    context->keys[ascKey] = 0;
    free(argv);
    return NULL;
}

void *$p(Core, handlePointerDown)(int argc, void **argv)
{
    if (argc < 4) {
        return NULL;
    }
    $$c(Context) *context = argv[0];

    $$c(Pointer) *pointer = $c(Pointer, findById)(context->pointers, 0);
    $_t(uhuge) x = *($_t(uhuge)*)(argv[1]),
               y = *($_t(uhuge)*)(argv[2]);
    unsigned char button = *(unsigned char*)argv[3];
    free(argv[1]);
    free(argv[2]);
    free(argv[3]);

    if (button > 2) {
        free(argv);
        return NULL;
    }
    pointer->x = x / (double)(context->width);
    pointer->y = 1 - y / (double)(context->height);
    pointer->button[button] = 1;
    if (context->processor->pointer_down) {
        context->processor->pointer_down(pointer);
    }
    free(argv);
    return NULL;
}

void *$p(Core, handlePointerMove)(int argc, void **argv)
{
    if (argc < 3) {
        return NULL;
    }
    $$c(Context) *context = argv[0];

    $$c(Pointer) *pointer = $c(Pointer, findById)(context->pointers, 0);
    $_t(uhuge) x = *($_t(uhuge)*)(argv[1]),
               y = *($_t(uhuge)*)(argv[2]);

    pointer->x = x / (double)(context->width);
    pointer->y = 1 - y / (double)(context->height);
    free(argv);
    return NULL;
}

void *$p(Core, handlePointerUp)(int argc, void **argv)
{
    if (argc < 4) {
        return NULL;
    }

    $$c(Context) *context = argv[0];

    $$c(Pointer) *pointer = $c(Pointer, findById)(context->pointers, 0);
    $_t(uhuge) x = *($_t(uhuge)*)(argv[1]),
        y = *($_t(uhuge)*)(argv[2]);
    unsigned char button = *(unsigned char*)argv[3];
    free(argv[1]);
    free(argv[2]);
    free(argv[3]);

    if (button > 2) {
        free(argv);
        return NULL;
    }
    pointer->x = x / (double)(context->width);
    pointer->y = 1 - y / (double)(context->height);
    pointer->button[button] = 0;
    if (context->processor->pointer_up) {
        context->processor->pointer_up(pointer);
    }
    free(argv);
    return NULL;
}

void $g(Call, onRender)(void *(*handler)(int argc, void **argv), int argc, void **argv)
{
    pthread_mutex_lock(&$_c(Handle, onRenderMutex));
    $c(List, push)($_g(Handler, onRender), $c(EventHandler, create)(handler, argc, argv));
    pthread_mutex_unlock(&$_c(Handle, onRenderMutex));
}

void $p(Call, onProcess)(void *(*handler)(int argc, void **argv), int argc, void **argv)
{
    pthread_mutex_lock(&$_c(Handle, onProcessMutex));
    $c(List, push)($_p(Handler, onProcess), $c(EventHandler, create)(handler, argc, argv));
    pthread_mutex_unlock(&$_c(Handle, onProcessMutex));
}

void $g(Handle, onRender)()
{
    pthread_mutex_lock(&$_c(Handle, onRenderMutex));

    while ($_g(Handler, onRender)->next) {
        $$c(EventHandler) *eventHandler= $_g(Handler, onRender)->value;
        if (eventHandler) {
            eventHandler->handler(eventHandler->argc, eventHandler->argv);
            free(eventHandler->argv);
            free(eventHandler);
        }
        $_g(Handler, onRender) = $_g(Handler, onRender)->next;
        free($_g(Handler, onRender)->prev);
        $_g(Handler, onRender)->prev = NULL;
    }

    pthread_mutex_unlock(&$_c(Handle, onRenderMutex));
}

void $p(Handle, onProcess)()
{
    pthread_mutex_lock(&$_c(Handle, onProcessMutex));

    while ($_p(Handler, onProcess)->next) {
        $$c(EventHandler) *eventHandler= $_p(Handler, onProcess)->value;
        if (eventHandler) {
            eventHandler->handler(eventHandler->argc, eventHandler->argv);
            free(eventHandler);
        }
        $_p(Handler, onProcess) = $_p(Handler, onProcess)->next;
        free($_p(Handler, onProcess)->prev);
        $_p(Handler, onProcess)->prev = NULL;
    }

    pthread_mutex_unlock(&$_c(Handle, onProcessMutex));
}

void $c(ContextSet, addContext)($$c(List) *set, char *key, $$c(Context) *context)
{
    $c(List, push)(set, $c(Item, create)(key, context));
}

$$c(Context) *$c(ContextSet, getContext)($$c(List) *set, char *key)
{
    $$c(Item) *item = $c(Item, get)(set, key);
    return item ? item->value : NULL;
}

void $c(ContextSet, free)($$c(List) *set)
{
    $c(Storage, free)(set);
}
#ifndef RHIC_ALL_CONTEXT_H
#define RHIC_ALL_CONTEXT_H

#include "../macro.h"

#define BILLION 1000000000UL
#define MAX_FRAME_RATE 25
#define MAX_PROC_RATE 50

extern const unsigned int frame_time, proc_time;

$$c(Context);
$$c(List);
$$c(Pointer);

pthread_mutex_t $_c(Context, displayMutex), $_c(Context, statusMutex);

pthread_mutex_t $_c(Handle, onRenderMutex), $_c(Handle, onProcessMutex);

$$c(ContextProcessor)
{
    int (*process_init)($$c(Context) *context);
    int (*process_free)($$c(Context) *context);
    int (*respond_init)($$c(Context) *context);
    int (*respond_free)($$c(Context) *context);
    int (*render_init)($$c(Context) *context);
    int (*render_free)($$c(Context) *context);
    int (*process)($$c(Context) *context);
    int (*render)($$c(Context) *context);
    void (*key_down)(unsigned char key);
    void (*key_up)(unsigned char key);
    void (*pointer_down)($$c(Pointer) *pointer);
    void (*pointer_up)($$c(Pointer) *pointer);
    void (*resize)($_t(uhuge) width, $_t(uhuge) height);
};


/**
 * @return Current time in nanoseconds
 */

$_t(uhuge) $c(Time, now)();


/**
 * Pauses current thread
 * @param nano Amount of time to pause the thread for
 */

void $c(Time, sleep)($_t(uhuge) nano);

void *$p(Core, handleKeyDown)(int argc, void **argv);
void *$p(Core, handleKeyUp)(int argc, void **argv);

void *$p(Core, handlePointerDown)(int argc, void **argv);
void *$p(Core, handlePointerMove)(int argc, void **argv);
void *$p(Core, handlePointerUp)(int argc, void **argv);

void $g(Call, onRender)(void *(*handler)(int argc, void **argv), int argc, void **argv);
void $p(Call, onProcess)(void *(*handler)(int argc, void **argv), int argc, void **argv);
void $g(Handle, onRender)();
void $p(Handle, onProcess)();


/**
 * Adds a context to the context set
 * @param set
 * @param key
 * @param scene
 */

void $c(ContextSet, addContext)($$c(List) *set, char *key, $$c(Context) *context);


/**
 * Gets a context from a set
 * @param set
 * @param key
 * @return
 */

$$c(Context) *$c(ContextSet, getContext)($$c(List) *set, char *key);


/**
 * Frees a context set
 * @param set
 */

void $c(ContextSet, free)($$c(List) *set);

$$c(List) *$_g(Handler, onRender);
$$c(List) *$_p(Handler, onProcess);

#endif //RHIC_ALL_CONTEXT_H

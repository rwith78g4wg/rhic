#include "list.h"
#include "storage.h"
#include "eventhandler.h"
#include "../gl/entity.h"

#include "object.h"

void $c(Object, fireEvent)($$c(Object) **object, $_t(uhuge) eventType, int argc, void **argv)
{
    if (!object || !*object) {
        return;
    }
    $$c(List) *entryGroups = $c(Object, findEntry)((*object)->type);
    if (!entryGroups) {
        return;
    }
    alloc_array(void*, nargv, argc + 1);
    *nargv = *object;
    if (argc && argv) {
        $aeach(void*, arg, argv, 0, argc) {
            *(nargv + arg_index + 1) = *arg;
        }
    }
    $c(EventHandler, fire)(entryGroups, eventType, argc + 1, nargv);
    $c(EventHandler, fire)((*object)->eventHandlerGroups, eventType, argc + 1, nargv);
    *object = nargv[0];
    free(nargv);
}

_Bool $c(Object, registerEntry)(char *type, $$c(List) *eventHandlerGroups)
{
    if ($c(Object, findEntry)(type)) {
        return 0;
    }

    $c(List, push)($_c(Object, entries), $c(Item, create)(type, eventHandlerGroups));

    return 1;
}

$$c(List) *$c(Object, findEntry)(char *type)
{
    $$c(Item) *item = $c(Item, get)($_c(Object, entries), type);
    return item ? item->value : NULL;
}

void $c(Object, freeEntries)()
{
    $$c(List) *entries = $c(List, begin)($_c(Object, entries));
    $each ($$c(Item), entry, entries) {
        $each ($$c(EventHandlerGroup), group, entry->value) {
            $c(List, free)(group->eventHandlers);
            free(group);
        }
        $c(List, free)(entry->value);
        free(entry);
    }
    $c(List, free)(entries);
}

$$c(Object) *$c(Object, create)(char *type, int argc, void **argv)
{
    $$c(List) *entryGroups = $c(Object, findEntry)(type);

    if (entryGroups) {
        alloc($$c(Object), object);
        object->argv = NULL;
        object->argc = 0;
        object->$$_p(active) = 1;
        object->$$_g(active) = 1;

        object->eventHandlerGroups = $c(List, create)();
        object->dynamicEntities = $c(List, create)();
        object->staticEntities = NULL;
        object->type = type;

        alloc_array(void*, nargv, argc + 1);
        *nargv = object;

        if (argc && argv) {
            $aeach(void*, arg, argv, 0, argc) {
                *(nargv + 1) = arg;
            }
        }

        $c(EventHandler, fire)(entryGroups, RHIC_OBJECT_EVENT_TYPE_CREATE, argc + 1, nargv);
        free(nargv);
        return object;
    }

    return NULL;
}

void $c(Object, free)($$c(Object) *object)
{
    $$c(List) *entryGroups = $c(Object, findEntry)(object->type);

    if (entryGroups) {
        $c(EventHandler, fire)(entryGroups, RHIC_OBJECT_EVENT_TYPE_DESTROY, 1, (void**)&object);
        if (object->staticEntities) {
            {
                $aeach ($$g(Entity)*, entity, object->staticEntities, 0, object->staticEntitiesNum) {
                    $g(Entity, free)(*entity);
                }
                free(object->staticEntities);
            }
            {
                $each ($$g(Entity), entity, object->dynamicEntities) {
                    $g(Entity, free)(entity);
                }
                $c(List, free)(object->dynamicEntities);
            }
            $each ($$c(List), eventHandlers, object->eventHandlerGroups) {
                $c(List, free)(eventHandlers);
            }
            free(object->eventHandlerGroups);
        }
    }
}
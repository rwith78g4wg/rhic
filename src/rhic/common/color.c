#include "color.h"

void $c(Color, brightenBy)(float *source, float *dest, float times)
{
    float lSr = source[0] * 0.2126;
    float lSg = source[1] * 0.7152;
    float lSb = source[2] * 0.0722;
    float lum = lSr + lSg + lSb;

    dest[0] = (lum * times) * source[0] / lum;
    dest[1] = (lum * times) * source[1] / lum;
    dest[2] = (lum * times) * source[2] / lum;

    if (dest[0] < 0) {
        dest[0] = 0;
    }
    if (dest[0] > 1) {
        dest[0] = 1;
    }
    if (dest[1] < 0) {
        dest[1] = 0;
    }
    if (dest[1] > 1) {
        dest[1] = 1;
    }
    if (dest[2] < 0) {
        dest[2] = 0;
    }
    if (dest[2] > 1) {
        dest[2] = 1;
    }
}

void $c(Color, saturateBy)(float *source, float *dest, float times)
{
    float lSr = source[0] * 0.2126;
    float lSg = source[1] * 0.7152;
    float lSb = source[2] * 0.0722;
    float lum = lSr + lSg + lSb;

    dest[0] = lum * (1 - times) + source[0] * times;
    dest[1] = lum * (1 - times) + source[1] * times;
    dest[2] = lum * (1 - times) + source[2] * times;

    if (dest[0] < 0) {
        dest[0] = 0;
    }
    if (dest[0] > 1) {
        dest[0] = 1;
    }
    if (dest[1] < 0) {
        dest[1] = 0;
    }
    if (dest[1] > 1) {
        dest[1] = 1;
    }
    if (dest[2] < 0) {
        dest[2] = 0;
    }
    if (dest[2] > 1) {
        dest[2] = 1;
    }
}
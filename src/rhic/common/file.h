#ifndef RHIC_FILE_H
#define RHIC_FILE_H
#include <stdio.h>

#include "macro.h"

#define RHIC_DATA_PATH "data/"

/**
 * Loads file and sets it's value to a variable.
 * Additionally macro creates _filesize postfixed variable which value equals to file size in bytes.
 */

#define fget(var, path) char* var = NULL;\
$_t(uhuge) var##_filesize = 0;\
{\
    FILE *var##_file = fopen(path, "rb");\
    if (var##_file) {\
        fseek(var##_file, 0, SEEK_END);\
        var##_filesize = ftell(var##_file);\
        fseek(var##_file, 0, SEEK_SET);\
        var = new_array(char, var##_filesize + 1);\
        if (fread(var, var##_filesize, 1, var##_file)) {}\
        fclose(var##_file);\
    }\
} var[var##_filesize] = '\0'

#endif //RHIC_FILE_H

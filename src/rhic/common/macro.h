/**
 * Macro header of Rhic. Contains declaration of namespace, memory management and array foreach macros
 */

#ifndef RHIC_BASE_MACRO_H
#define RHIC_BASE_MACRO_H

#include <string.h>
#include <stdlib.h>

/**
 * Allocates a memory for a required amount of elements of given type and creates a pointer to this memory
 */

#define alloc_array(type, name, size) type *name = malloc(sizeof(type) * size)


/**
 * Allocates a memory for a required amount of elements of given type
 */

#define new_array(type, size) malloc(sizeof(type) * size)


/**
 * Copies value of the memory occupied by the variable into another one
 */

#define copy(from, to) memcpy(to, from, sizeof(typeof(from)))


/**
 * Allocates a memory for one element of given type and creates a pointer to this memory
 */

#define alloc(type, name) type *name = malloc(sizeof(type))


/**
 * Allocates a memory for one element of given type
 */

#define new(type) malloc(sizeof(type))


/**
 * Allocates a memory for one element of given type, creates a pointer to this memory and allows to set it's value
 */

#define salloc(type, name) type *name = malloc(sizeof(type)); *name


/**
 * Fills given size of the memory starting from a pointer with a given value
 */

#define fill(what, size, with) memset(what, with, sizeof(typeof(*what)) * size)


/**
 * Generic namespace macro
 *
 * Namespace must have the same name of the object/structure/activity it's identifier is working with.
 */

#define $(origin, space, name) Rhic_##origin##_##space##_##name


/**
 * Generic namespace macro for libraries
 */

#define $l(origin, space, name) rhic_Library_##space##_##name


/**
 * These are shortened namespace macros for both application and libraries.
 *
 * Identifier must have Graphics origin if it is meant to be used in render thread.
 * Identifier must have Process origin if it is meant to be used in process thread.
 * Identifier must have Audio origin if it works with audio playback.
 * Identifier must have Network origin if it is responsible for network activity.
 *
 * Identifier must have Common origin if it may be used at least in two of the situations given above.
 */

#define $c(space, name) Rhic_Common_##space##_##name
#define $g(space, name) Rhic_Graphics_##space##_##name
#define $p(space, name) Rhic_Process_##space##_##name
#define $a(space, name) Rhic_Audio_##space##_##name
#define $n(space, name) Rhic_Network_##space##_##name

#define $lc(space, name) Rhic_Library_Common_##space##_##name
#define $lg(space, name) Rhic_Library_Graphics_##space##_##name
#define $lp(space, name) Rhic_Library_Process_##space##_##name
#define $la(space, name) Rhic_Library_Audio_##space##_##name
#define $ln(space, name) Rhic_Library_Network_##space##_##name


/**
 * Namespace macros for variables/constants
 */

#define $_(origin, space, name) Rhic_Identifier_##origin##_##space##_##name

#define $_l(origin, space, name) rhic_Library_Identifier_##space##_##name

#define $_c(space, name) Rhic_Common_Identifier_##space##_##name
#define $_g(space, name) Rhic_Graphics_Identifier_##space##_##name
#define $_p(space, name) Rhic_Process_Identifier_##space##_##name
#define $_a(space, name) Rhic_Audio_Identifier_##space##_##name
#define $_n(space, name) Rhic_Network_Identifier_##space##_##name

#define $_lc(space, name) Rhic_Library_Identifier_Common_##space##_##name
#define $_lg(space, name) Rhic_Library_Identifier_Graphics_##space##_##name
#define $_lp(space, name) Rhic_Library_Identifier_Process_##space##_##name
#define $_la(space, name) Rhic_Library_Identifier_Audio_##space##_##name
#define $_ln(space, name) Rhic_Library_Identifier_Network_##space##_##name

#define $$_c(name) Rhic_Common_Identifier_Attribute_##name
#define $$_g(name) Rhic_Graphics_Identifier_Attribute_##name
#define $$_p(name) Rhic_Process_Identifier_Attribute_##name
#define $$_a(name) Rhic_Audio_Identifier_Attribute_##name
#define $$_n(name) Rhic_Network_Identifier_Attribute_##name

#define $$_lc(name) Rhic_Library_Identifier_Common_Attribute_##name
#define $$_lg(name) Rhic_Library_Identifier_Graphics_Attribute_##name
#define $$_lp(name) Rhic_Library_Identifier_Process_Attribute_##name
#define $$_la(name) Rhic_Library_Identifier_Audio_Attribute_##name
#define $$_ln(name) Rhic_Library_Identifier_Network_Attribute_##name


/**
 * This is a macro for type definitions.
 * It must be used only to unify similar platform specific types under one name.
 */

#define $_t(name) Rhic_Type_##name

/**
 * Those are namespace macros for structure objects
 */

#define $$(origin, name) struct Rhic_Struct_##origin##_##name
#define $$l(origin, name) struct Rhic_Struct_Library_##name

#define $$c(name) struct Rhic_Struct_Common_##name
#define $$g(name) struct Rhic_Struct_Graphics_##name
#define $$p(name) struct Rhic_Struct_Process_##name
#define $$a(name) struct Rhic_Struct_Audio_##name
#define $$n(name) struct Rhic_Struct_Network_##name

#define $$lc(name) struct Rhic_Struct_Library_Common_##name
#define $$lg(name) struct Rhic_Struct_Library_Graphics_##name
#define $$lp(name) struct Rhic_Struct_Library_Process_##name
#define $$la(name) struct Rhic_Struct_Library_Audio_##name
#define $$ln(name) struct Rhic_Struct_Library_Network_##name


/**
 * Array foreach macro. Generates for loop with step = 1 from <from> to <to> excluding last value.
 */

#define $aeach(type, name, array, from, to) \
type * name = (type*)array;\
for ($_t(uhuge) name##_index = from; name##_index < to; name##_index++, name++)

#ifdef __unix__

typedef ssize_t $_t(huge);
typedef size_t $_t(uhuge);

#else

typedef unsigned long $_t(huge);

#endif

#endif //RHIC_BASE_MACRO_H

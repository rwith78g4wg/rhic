#ifndef RHIC_COMMON_THREAD_H
#define RHIC_COMMON_THREAD_H

#include <pthread.h>

#include "macro.h"

typedef pthread_mutex_t $_t(mutex);
typedef pthread_t $_t(thread);

#define $mutex_init(mutex) pthread_mutex_init(&mutex, NULL)
#define $mutex_free(mutex) pthread_mutex_destroy(&mutex)

#define $mute(mutex) pthread_mutex_lock(&mutex)
#define $unmute(mutex) pthread_mutex_unlock(&mutex)


$_t(thread) *$detach(void *(*handler)(void *argv), void **argv);
void $sync($_t(thread) *thread);

#endif //RHIC_COMMON_THREAD_H

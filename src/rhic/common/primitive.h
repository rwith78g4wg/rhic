#ifndef RHIC_COMMON_PRIMITIVE_H
#define RHIC_COMMON_PRIMITIVE_H

#include "macro.h"

$$c(Rect)
    {
        int
        left,
        top,
        right,
        bottom;
    };

$$c(Rect) $c(Rect, create)(int left, int top, unsigned int right, unsigned int bottom);



$$c(UPoint)
    {
        unsigned int
        x,
        y;
    };

$$c(UPoint) $c(UPoint, create)(unsigned int x, unsigned int y);



$$c(Point)
    {
        int
        x,
        y;
    };

$$c(Point) $c(Point, create)(int x, int y);


#endif //RHIC_COMMON_PRIMITIVE_H

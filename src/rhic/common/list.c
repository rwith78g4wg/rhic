#include "list.h"

$$c(List) *$c(List, begin)($$c(List) *list)
{
    if (!list) {
        return NULL;
    }
    if (!list->prev) {
        return list;
    }
    while (list->prev) {
        list = list->prev;
    }
    return list;
}

$$c(List) *$c(List, end)($$c(List) *list)
{
    if (!list) {
        return NULL;
    }
    while (list->next) {
        list = list->next;
    }
    return list;
}

$$c(List) **$c(List, prat)($$c(List) *list, int i)
{
    if (!list) {
        return NULL;
    }
    $$c(List) **plist = &list;
    while ((*plist)->prev && --i) {
        *plist = (*plist)->prev;
    }
    return i ? NULL : plist;
}

$$c(List) **$c(List, pat)($$c(List) *list, int i)
{
    if (!list) {
        return NULL;
    }
    $$c(List) **plist = &list;
    while ((*plist)->next && --i) {
        *plist = (*plist)->next;
    }
    return i ? NULL : plist;
}

$$c(List) *$c(List, rat)($$c(List) *list, int i)
{
    if (!list) {
        return NULL;
    }
    while (list->prev && --i) {
        list = list->prev;
    }
    return i ? NULL : list;
}

$$c(List) *$c(List, at)($$c(List) *list, int i)
{
    if (!list) {
        return NULL;
    }
    while (list->next && --i) {
        list = list->next;
    }
    return i ? NULL : list;
}

$_t(uhuge) $c(List, right)($$c(List) *list)
{
    if (!list) {
        return 0;
    }

    $_t(uhuge) i = 0;
    while (list->next) {
        list = list->next;
        i++;
    }
    return i;
}

$_t(uhuge) $c(List, left)($$c(List) *list)
{
    if (!list) {
        return 0;
    }

    $_t(uhuge) i = 0;
    while (list->prev) {
        list = list->prev;
        i++;
    }
    return i;
}

$_t(uhuge) $c(List, size)($$c(List) *list)
{
    return $c(List, right)($c(List, begin)(list));
}

$$c(List) *$c(List, create)()
{
    alloc($$c(List), list);
    list->prev = NULL;
    list->next = NULL;
    list->value = NULL;

    return list;
}

void *$c(List, cutRight)($$c(List) **list)
{
    if (!*list) {
        return NULL;
    }
    void *value = (*list)->value;
    $c(List, freeRight)(list);
    if (*list) {
        (*list)->next = $c(List, create)();
        (*list)->next->prev = *list;
    } else {
        *list = $c(List, create)();
    }
    return value;
}

void *$c(List, pop)($$c(List) **list)
{
    if (!list) {
        return NULL;
    }
    $$c(List) *beforeEnd = $c(List, end)(*list)->prev;
    $$c(List) *dBeforeEnd = beforeEnd;
    void *value = $c(List, cutRight)(&beforeEnd);
    if (*list == dBeforeEnd) {
        *list = beforeEnd;
    }
    return value;
}

void *$c(List, cutLeft)($$c(List) *list)
{
    if (!list || !list->prev) {
        return NULL;
    }
    void *value = list->prev->value;
    $c(List, freeLeft)(list);
    return value;
}

void *$c(List, shift)($$c(List) **list)
{
    if (!*list) {
        return NULL;
    }
    $$c(List) *afterBegin = $c(List, begin)(*list)->next;
    if (afterBegin) {
        *list = afterBegin;
        return $c(List, cutLeft)(afterBegin);
    } else {
        return NULL;
    }
}

void $c(List, freeRight)($$c(List) **list)
{
    if (!*list) {
        return;
    }
    $$c(List) *toFree = *list;
    *list = toFree->prev;
    while (toFree) {
        $$c(List) *next = toFree->next;
        free(toFree);
        toFree = next;
    }
    if (*list) {
        (*list)->next = NULL;
    }
}

void $c(List, freeLeft)($$c(List) *list)
{
    if (!list) {
        return;
    }
    $$c(List) *toFree = list->prev;
    while (toFree) {
        $$c(List) *prev = toFree->prev;
        free(toFree);
        toFree = prev;
    }
    list->prev = NULL;
}

void $c(List, free)($$c(List) *list)
{
    list = $c(List, begin)(list);
    $c(List, freeRight)(&list);
}

void $c(List, clear)($$c(List) **list)
{
    /**
     * Difference between this method and $c(List, free) is that $c(List, freeRight) frees all
     * elements at right and the element itself while $c(List, freeLeft) only frees elements at left
     * so removing all elements except the last (which is always empty) clears the list.
     */
    *list = $c(List, end)(*list);
    $c(List, freeLeft)(*list);
}

$$c(List) *$c(List, setNext)($$c(List) *list, void *value)
{
    if(!list) {
        return NULL;
    }
    list->value = value;
    list->next = $c(List, create)();
    list->next->prev = list;
    return list;
}

$$c(List) *$c(List, push)($$c(List) *list, void *value)
{
    if(!list) {
        return NULL;
    }
    list = $c(List, end)(list);
    return $c(List, setNext)(list, value);
}

$$c(List) *$c(List, setPrev)($$c(List) *list, void *value)
{
    if(!list) {
        return NULL;
    }
    list->prev = $c(List, create)();
    list->prev->next = list;
    list->prev->value = value;
    return list->prev;
}

$$c(List) *$c(List, unshift)($$c(List) *list, void *value)
{
    if(!list) {
        return NULL;
    }
    list = $c(List, begin)(list);
    return $c(List, setPrev)(list, value);
}

void *$c(List, remove)($$c(List) **list, $$c(List) **item)
{
    if (!*item || !(*item)->next) {
        return NULL;
    }

    $$c(List) *next = (*item)->next;
    next->prev = (*item)->prev;
    if ((*item)->prev) {
        (*item)->prev->next = next;
    }
    void *value = (*item)->value;
    free(*item);
    *item = next;
    if (*item == *list) {
        *list = next;
    }
    return value;
}

void *$c(List, insertAfter)($$c(List) *list, void *values, $_t(uhuge) num)
{
    if(!list) {
        return NULL;
    }
    $_t(uhuge) sizeOfValue = sizeof(typeof(values)) / 2;
    for ($_t(uhuge) i = 0; i < num; i++) {
        list->value = values;
        values += sizeOfValue;
        list->next = $c(List, create)();
        list->next->prev = list;
        list = list->next;
    }
    return list;
}

void *$c(List, insertAsLast)($$c(List) *list, void *values, $_t(uhuge) num)
{
    return $c(List, insertAfter)($c(List, end)(list), values, num);
}

void *$c(List, insertBefore)($$c(List) *list, void *values, $_t(uhuge) num)
{
    if(!list) {
        return NULL;
    }
    $_t(uhuge) sizeOfValue = sizeof(typeof(values)) / 2;
    values += sizeOfValue * (num - 1);
    for ($_t(uhuge) i = 0; i < num; i++) {
        list->prev = $c(List, create)();
        list->prev->next = list;
        list = list->prev;
        list->value = values;
        values -= sizeOfValue;
    }
    return list;
}

void *$c(List, insertAsFirst)($$c(List) *list, void *values, $_t(uhuge) num)
{
    return $c(List, insertBefore)($c(List, begin)(list), values, num);
}
#include "list.h"

#include "pointer.h"

$$c(Pointer) *$c(Pointer, add)($$c(List) *pointers, unsigned char id, double x, double y)
{
    $each ($$c(Pointer), pointer, pointers) {
        if (pointer->id == id) {
            return pointer;
        }
    }
    pointer = new($$c(Pointer));

    pointer->button = new_array(_Bool, 3);
    pointer->button[0] = pointer->button[1] = pointer->button[2] = 0;
    pointer->id = id;
    pointer->x = x;
    pointer->y = y;

    $c(List, push)(pointers, pointer);

    return pointer;
}

void $c(Pointer, free)($$c(Pointer) *pointer)
{
    free(pointer->button);
    free(pointer);
}

$$c(Pointer) *$c(Pointer, findContained)($$c(List) *pointers, unsigned char button, double left, double top, double right, double bottom)
{
    $each ($$c(Pointer), pointer, pointers) {
        if ((button == 255 || pointer->button[button])
            && pointer->x >= left
            && pointer->y >= top
            && pointer->x <= right
            && pointer->y <= bottom
        ) {
            return pointer;
        }
    }
    return NULL;
}

$$c(Pointer) *$c(Pointer, findById)($$c(List) *pointers, unsigned char id)
{
    $each ($$c(Pointer), pointer, pointers) {
        if (pointer->id == id) {
            return pointer;
        }
    }
    return NULL;
}

$$c(Pointer) *$c(Pointer, findByRule)($$c(List) *pointers, _Bool (*callback)($$c(Pointer)* pointer))
{
    $each ($$c(Pointer), pointer, pointers) {
        if (callback(pointer)) {
            return pointer;
        }
    }
    return NULL;
}
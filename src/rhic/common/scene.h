#ifndef RHIC_COMMON_SCENE_H
#define RHIC_COMMON_SCENE_H

#include "thread.h"

$$c(Object);
$$c(Rect);

#define RHIC_SCENE_EVENT_TYPE_REFRESH_PROJECTION 0
#define RHIC_SCENE_EVENT_TYPE_REFRESH_VIEW 1


/**
 * Scene is an object which contains its objects and a view matrix
 */

$$c(Scene)
{
    float *viewMatrix;

    $_t(mutex) objectMutex;

    $$c(List) *renderObjects;
    $$c(List) *processObjects;
    $$c(List) *allObjects;
    $$c(List) *eventHandlerGroups;

    $$c(Rect) *scissor;
    float *background;
    _Bool useDepth;
};


/**
 * Scene set is an object which contains its scenes and a projection matrix
 */

$$c(SceneSet)
{
    float *projectionMatrix;

    $$c(List) *scenes;
    $$c(List) *eventHandlerGroups;
};


/**
 * Creates a new scene
 * @return
 */

$$c(Scene) *$c(Scene, create)();


/**
 * Sends a process event to all objects of the scene
 * @param scene
 */

void $p(Scene, process)($$c(Scene) *scene);


/**
 * Sends a render event to all objects of the scene
 * @param scene
 */

void $g(Scene, render)($$c(Scene) *scene, float *projectionMatrix);


/**
 * Sends an object to the scene
 * @param scene
 */

void $p(Scene, addObject)($$c(Scene) *scene, $$c(Object) *object);


/**
 * Removes an object from the scene
 * @param scene
 */

void $p(Scene, removeObject)($$c(Scene) *scene, $$c(Object) *object);


/**
 * Frees a scene
 * @param scene
 */

void $c(Scene, free)($$c(Scene) *scene);


/**
 * Fires an event related to the scene
 * @param scene
 */

void $c(Scene, fireEvent)($$c(Scene) **scene, $_t(uhuge) eventType, int argc, void **argv);


/**
 * Creates a scene set
 * @return
 */

$$c(SceneSet) *$c(SceneSet, create)();


/**
 * Adds a scene to the scene set
 * @param set
 * @param key
 * @param scene
 */

void $c(SceneSet, addScene)($$c(SceneSet) *set, char *key, $$c(Scene) *scene);


/**
 * Removes a scene from the scene set
 * @param set
 * @param key
 */

void $c(SceneSet, removeScene)($$c(SceneSet) *set, char *key);


/**
 * Gets a scene from a set
 * @param set
 * @param key
 * @return
 */

$$c(Scene) *$c(SceneSet, getScene)($$c(SceneSet) *set, char *key);


/**
 * Sends a process event to all scenes of the set
 * @param set
 */

void $p(SceneSet, process)($$c(SceneSet) *set);


/**
 * Sends a render event to all scenes of the set
 * @param set
 */

void $g(SceneSet, render)($$c(SceneSet) *set);


/**
 * Frees a scene set
 * @param set
 */

void $c(SceneSet, free)($$c(SceneSet) *set);


/**
 * Fires an event related to the scenes of the set
 * @param set
 */

void $c(SceneSet, fireSceneEvent)($$c(SceneSet) **set, $_t(uhuge) eventType, int argc, void **argv);


/**
 * Fires an event related to the objects of the set
 * @param set
 */

void $c(SceneSet, fireObjectEvent)($$c(SceneSet) *set, $_t(uhuge) eventType, int argc, void **argv);

#endif //RHIC_COMMON_SCENE_H

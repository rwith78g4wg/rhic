#include <GL/glew.h>

#include "primitive.h"
#include "list.h"
#include "object.h"
#include "storage.h"
#include "eventhandler.h"

#include "scene.h"

$$c(Scene) *$c(Scene, create)()
{
    alloc($$c(Scene), scene);
    scene->allObjects = $c(List, create)();
    scene->processObjects = $c(List, create)();
    scene->renderObjects = $c(List, create)();
    scene->eventHandlerGroups = $c(List, create)();
    scene->viewMatrix = new_array(float, 16);
    scene->background = NULL;
    scene->scissor = NULL;
    scene->useDepth = 0;
    $mutex_init(scene->objectMutex);
    fill(scene->viewMatrix, 16, 0);
    return scene;
}

void $p(Scene, process)($$c(Scene) *scene)
{
    {
        $$c(List) *newObjects = $c(List, create)();

        {
            $each ($$c(Object), object, scene->processObjects) {
                if (object->$$_p(active)) {
                    $c(Object, fireEvent)(&object, RHIC_OBJECT_EVENT_TYPE_PROCESS, 0, NULL);
                    $c(List, push)(newObjects, object);
                } else {
                    $mute(scene->objectMutex);
                    object->$$_p(active) = -1;
                    object->$$_g(active) = 0;
                    $unmute(scene->objectMutex);
                }
            }
        }

        $mute(scene->objectMutex);

        $$c(List) *oldObjects = scene->processObjects;
        scene->processObjects = newObjects;

        $unmute(scene->objectMutex);

        $c(List, free)(oldObjects);
    }

    {
        $$c(List) *newObjects = $c(List, create)();
        $$c(List) *deleteObjects = $c(List, create)();

        {
            $each ($$c(Object), object, scene->allObjects) {
                if (object->$$_p(active) == -1 && object->$$_g(active) == -1) {
                    $c(List, push)(deleteObjects, object);
                } else {
                    $c(List, push)(newObjects, object);
                }
            }
        }

        $$c(List) *oldObjects = scene->allObjects;
        scene->allObjects = newObjects;

        {
            $each ($$c(Object), object, deleteObjects) {
                $c(Object, free)(object);
            }
        }

        $c(List, free)(deleteObjects);
        $c(List, free)(oldObjects);
    }
}

void $g(Scene, render)($$c(Scene) *scene, float *projectionMatrix)
{
    if (scene->scissor) {
        glEnable(GL_SCISSOR_TEST);
        glScissor(
            scene->scissor->left,
            scene->scissor->bottom,
            scene->scissor->right - scene->scissor->left,
            scene->scissor->top - scene->scissor->bottom
        );
    } else {
        glDisable(GL_SCISSOR_TEST);
    }
    if (scene->background) {
        glClearColor(scene->background[0], scene->background[1], scene->background[2], scene->background[3]);
        glClear(GL_COLOR_BUFFER_BIT);
    }
    if (scene->useDepth) {
        glEnable(GL_DEPTH);
        glClear(GL_DEPTH_BUFFER_BIT);
    } else {
        glDisable(GL_DEPTH);
    }
    int argc;
    void **argv;
    if (projectionMatrix) {
        argc = 2;
        argv = new_array(void*, argc);
        argv[0] = projectionMatrix;
        argv[1] = scene->viewMatrix;
    } else {
        argc = 1;
        argv = new_array(void*, argc);
        argv[0] = scene->viewMatrix;
    }

    {
        $$c(List) *newObjects = $c(List, create)();

        {
            $each($$c(Object), object, scene->renderObjects) {
                if (object->$$_g(active)) {
                    $c(Object, fireEvent)(&object, RHIC_OBJECT_EVENT_TYPE_RENDER, argc, argv);
                    $c(List, push)(newObjects, object);
                } else {
                    $mute(scene->objectMutex);
                    object->$$_g(active) = -1;
                    object->$$_p(active) = 0;
                    $unmute(scene->objectMutex);
                }
            }
        }

        $mute(scene->objectMutex);

        $$c(List) *oldObjects = scene->renderObjects;
        scene->renderObjects = newObjects;

        $unmute(scene->objectMutex);

        $c(List, free)(oldObjects);
    }
}

void $p(Scene, addObject)($$c(Scene) *scene, $$c(Object) *object)
{
    $mute(scene->objectMutex);
    object->$$_p(active) = 1;
    object->$$_g(active) = 1;
    $c(List, push)(scene->allObjects, object);
    $c(List, push)(scene->processObjects, object);
    $c(List, push)(scene->renderObjects, object);
    $unmute(scene->objectMutex);
}

void $c(Scene, free)($$c(Scene) *scene)
{
    $each ($$c(List), eventHandlers, scene->eventHandlerGroups) {
        $c(List, free)(eventHandlers);
    }
    free(scene->eventHandlerGroups);
    $each($$c(Object), object, scene->allObjects) {
        $c(Object, free)(object);
    }
    $c(List, free)(scene->renderObjects);
    $c(List, free)(scene->processObjects);
    $c(List, free)(scene->allObjects);
    $mutex_free(scene->objectMutex);
    free(scene->viewMatrix);
    free(scene->background);
    free(scene);
}

void $c(Scene, fireEvent)($$c(Scene) **scene, $_t(uhuge) eventType, int argc, void **argv)
{
    if (!scene || !*scene) {
        return;
    }
    alloc_array(void*, nargv, argc + 1);
    *nargv = *scene;
    if (argc && argv) {
        $aeach(void*, arg, argv, 0, argc) {
            *(nargv + arg_index + 1) = *arg;
        }
    }
    $c(EventHandler, fire)((*scene)->eventHandlerGroups, eventType, argc + 1, nargv);
    *scene = nargv[0];
    free(nargv);
}


$$c(SceneSet) *$c(SceneSet, create)()
{
    alloc($$c(SceneSet), set);
    set->scenes = $c(List, create)();
    set->eventHandlerGroups = $c(List, create)();
    set->projectionMatrix = new_array(float, 16);
    fill(set->projectionMatrix, 16, 0);
    return set;
}

void $c(SceneSet, addScene)($$c(SceneSet) *set, char *key, $$c(Scene) *scene)
{
    $c(List, push)(set->scenes, $c(Item, create)(key, scene));
}

$$c(Scene) *$c(SceneSet, getScene)($$c(SceneSet) *set, char *key)
{
    $$c(Item)* item = $c(Item, get)(set->scenes, key);
    return item ? item->value : NULL;
}

void $p(SceneSet, process)($$c(SceneSet) *set)
{
    $each ($$c(Item), item, set->scenes) {
        $p(Scene, process)(item->value);
    }
}

void $g(SceneSet, render)($$c(SceneSet) *set)
{
    $each ($$c(Item), item, set->scenes) {
        $g(Scene, render)(item->value, set->projectionMatrix);
    }
}

void $c(SceneSet, free)($$c(SceneSet) *set)
{
    $each ($$c(List), eventHandlers, set->eventHandlerGroups) {
        $c(List, free)(eventHandlers);
    }
    free(set->eventHandlerGroups);
    $each ($$c(Item), item, set->scenes) {
        $c(Scene, free)(item->value);
    }
    $c(Storage, free)(set->scenes);
    free(set->projectionMatrix);
    free(set);
}

void $c(SceneSet, fireSceneEvent)($$c(SceneSet) **set, $_t(uhuge) eventType, int argc, void **argv)
{
    alloc_array(void*, nargv, argc + 1);
    *nargv = set;
    if (argc && argv) {
        $aeach(void*, arg, argv, 0, argc) {
            *(nargv + arg_index + 1) = *arg;
        }
    }
    $each ($$c(Item), item, (*set)->scenes) {
        $c(EventHandler, fire)((*set)->eventHandlerGroups, eventType, argc + 1, nargv);
        $$c(Scene) *scene = item->value;
        $c(Scene, fireEvent)(&scene, eventType, argc + 1, nargv);
    }
    free(nargv);
}

void $c(SceneSet, fireObjectEvent)($$c(SceneSet) *set, $_t(uhuge) eventType, int argc, void **argv)
{
    $each ($$c(Item), item, set->scenes) {
        $each ($$c(Object), object, (($$c(Scene)*)(item->value))->allObjects) {
            $c(Object, fireEvent)(&object, eventType, argc, argv);
        }
    }
}
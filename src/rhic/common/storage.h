#ifndef RHIC_STORAGE_H
#define RHIC_STORAGE_H

#include "macro.h"

$$c(List);

/**
 * Storage item object. It is used when one needs an associated list.
 */

$$c(Item) {
    char *key;
    void *value;
};


/**
 * Creates an item from a key and a value
 * @param key
 * @param value
 * @return
 */

$$c(Item) *$c(Item, create)(char *key, void *value);


/**
 * Gets an item from a storage by its key
 * @param storage
 * @param key
 * @return
 */

void *$c(Item, get)($$c(List) *storage, char *key);


/**
 * Frees an item object
 * @param item
 * @return
 */

void *$c(Item, free)($$c(Item) *item);


/**
 * Frees a storage and all items inside it
 * @param storage
 */

void $c(Storage, free)($$c(List) *storage);

#endif //RHIC_STORAGE_H

#include "primitive.h"

$$c(Rect) $c(Rect, create)(int left, int top, unsigned int right, unsigned int bottom)
{
    $$c(Rect) result;
    result.left = left;
    result.top = top;
    result.right = right;
    result.bottom = bottom;

    return result;
}

$$c(UPoint) $c(UPoint, create)(unsigned int x,unsigned int y)
{
    $$c(UPoint) result;
    result.x = x;
    result.y = y;

    return result;
}

$$c(Point) $c(Point, create)(int x,int y)
{
    $$c(Point) result;
    result.x = x;
    result.y = y;

    return result;
}
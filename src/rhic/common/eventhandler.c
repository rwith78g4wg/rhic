#include "list.h"

#include "eventhandler.h"

$$c(EventHandler) *$c(EventHandler, create)(void *(*handler)(int argc, void **argv), int argc, void **argv)
{
    alloc($$c(EventHandler), eventHandler);
    eventHandler->handler = handler;
    eventHandler->argc = argc;
    eventHandler->argv = argv;

    return eventHandler;
}

void $c(EventHandler, register)($$c(List) *eventGroups, $_t(uhuge) eventType, void *(*handler)(int argc, void **argv), int argc, void **argv)
{
    $$c(EventHandler) *eventHandler = $c(EventHandler, create)(handler, argc, argv);

    $each ($$c(EventHandlerGroup), group, eventGroups) {
        if (group->eventType == eventType) {
            $c(List, push)(group->eventHandlers, eventHandler);
            return;
        }
    }
    group = new($$c(EventHandlerGroup));
    group->eventType = eventType;
    group->eventHandlers = $c(List, create)();
    $c(List, push)(group->eventHandlers, eventHandler);
    $c(List, push)(eventGroups, group);
}

void $c(EventHandler, fire)($$c(List) *eventGroups, $_t(uhuge) eventType, int argc, void **argv)
{
    $each($$c(EventHandlerGroup), group, eventGroups) {
        if (group->eventType == eventType) {
            $each ($$c(EventHandler), event, ($$c(List)*)(group->eventHandlers)) {
                event->handler(argc, argv);
            }
        }
    }
}

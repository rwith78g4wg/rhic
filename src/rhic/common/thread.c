#include "thread.h"

$_t(thread) *$detach(void *(*handler)(void *argv), void **argv)
{
    alloc($_t(thread), thread);
    pthread_create(thread, NULL, handler, argv);
    return thread;
}

void $sync($_t(thread) *thread)
{
    pthread_join(*thread, NULL);
    free(thread);
}
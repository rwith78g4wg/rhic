#include "file.h"
#include "list.h"
#include "storage.h"

#include "rhg.h"

#define rhg_get_1(variable, content) variable = ($_t(uhuge))(content[0]); content++
#define rhg_get_2(variable, content) variable = ($_t(uhuge))(content[0] * 256) + content[1]; content += 2
#define rhg_get_3(variable, content) variable = (($_t(uhuge))(content[0] * 256) + content[1]) * 256 + content[2]; content += 3
#define rhg_get_4(variable, content) variable = ((($_t(uhuge))(content[0] * 256) + content[1]) * 256 + content[2]) * 256 + content[3]; content += 4

$$g(Rhg) *$g(Rhg, get)(char *name)
{
    $$c(Item) *item = $c(Item, get)($_c(Storage, rhg), name);
    if (item) {
        return item->value;
    }
    alloc_array(char, fileName, strlen(name) + 13 + strlen(RHIC_DATA_PATH));
    strcpy(fileName, RHIC_DATA_PATH "textures/");
    strcat(fileName, name);
    strcat(fileName, ".rhg");
    fget(sRhg, fileName);
    free(fileName);
    $$g(Rhg) *obRhg = $g(Rhg, load)(sRhg);
    free(sRhg);
    $c(List, push)($_c(Storage, rhg), $c(Item, create)(name, obRhg));
    return obRhg;
}

$$g(Rhg) *$g(Rhg, load)(unsigned char *content)
{
    alloc($$g(Rhg), rhg);
    unsigned char mode, sizing;

    rhg_get_1(mode, content);
    rhg_get_1(sizing, content);

    $_t(uhuge) height;

    rhg_get_4(rhg->width, content);
    rhg_get_4(height, content);

    rhg->data_size = rhg->width * height;
    rhg->data = new_array($_t(uhuge), rhg->data_size);

    rhg_get_4(rhg->palette_num, content);
    rhg->palette = new_array(unsigned char*, rhg->palette_num);

    $_t(uhuge) color_num;
    rhg_get_4(color_num, content);
    color_num *= 4;

    switch (mode) {
        case 0x00:
            for ($_t(uhuge) i = 0; i < rhg->data_size; i++) {
                switch (sizing) {
                    case 1:
                        rhg_get_1(rhg->data[i], content);
                        break;
                    case 2:
                        rhg_get_2(rhg->data[i], content);
                        break;
                    case 3:
                        rhg_get_3(rhg->data[i], content);
                        break;
                    case 4:
                        rhg_get_4(rhg->data[i], content);
                        break;
                }
            }
            break;
        case 0x01:
            {
                $_t(uhuge) gotData = 0;
                do {
                    $_t(uhuge) size = 0;

                    switch (sizing) {
                        case 1:
                        rhg_get_1(size, content);
                            break;
                        case 2:
                        rhg_get_2(size, content);
                            break;
                        case 3:
                        rhg_get_3(size, content);
                            break;
                        case 4:
                        rhg_get_4(size, content);
                            break;
                    }

                    $_t(uhuge) value = 0;

                    switch (sizing) {
                        case 1:
                        rhg_get_1(value, content);
                            break;
                        case 2:
                        rhg_get_2(value, content);
                            break;
                        case 3:
                        rhg_get_3(value, content);
                            break;
                        case 4:
                        rhg_get_4(value, content);
                            break;
                    }

                    for ($_t(uhuge) i = 0; i < size; i++) {
                        rhg->data[gotData + i] = value;
                    }

                    gotData += size;
                } while (gotData < rhg->data_size);
            }
            break;
    }

    for ($_t(uhuge) palette_index = 0; palette_index < rhg->palette_num; palette_index++) {
        rhg->palette[palette_index] = new_array(unsigned char, color_num);

        for ($_t(uhuge) i = 0; i < color_num; i += 4) {
            rhg_get_1(rhg->palette[palette_index][i], content);
            rhg_get_1(rhg->palette[palette_index][i + 1], content);
            rhg_get_1(rhg->palette[palette_index][i + 2], content);
            rhg_get_1(rhg->palette[palette_index][i + 3], content);
        }
    }

    return rhg;
}

unsigned char *$g(Rhg, apply)($$g(Rhg) *rhg, $_t(uhuge) index)
{
    unsigned char *data = new_array(unsigned char, rhg->data_size * 4);

    for ($_t(uhuge) i = 0; i < rhg->data_size; i++) {
        $_t(uhuge) il4 = i * 4;
        unsigned int ir4 = (unsigned int)(rhg->data[i] * 4);
        data[il4]     = rhg->palette[index][ir4];
        data[il4 + 1] = rhg->palette[index][ir4 + 1];
        data[il4 + 2] = rhg->palette[index][ir4 + 2];
        data[il4 + 3] = rhg->palette[index][ir4 + 3];
    }

    return data;
}

void $g(Rhg, free)($$g(Rhg) *rhg)
{
    $aeach (unsigned char**, palette, rhg->palette, 0, rhg->palette_num) {
        free(*palette);
    }
    free(rhg->palette);
    free(rhg->data);
    free(rhg);
}

#undef rhg_get_4
#undef rhg_get_3
#undef rhg_get_2
#undef rhg_get_1

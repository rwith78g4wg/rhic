#ifdef __unix__
#include <X11/Xatom.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <string.h>
#include <pthread.h>

#include "../list.h"
#include "../object.h"
#include "../storage.h"
#include "../pointer.h"
#include "../rhg.h"

#include "context.h"

$$g(Texture);
$$c(SceneSet);

extern $$c(List) *$_c(Storage, rhg),
                 *$_c(Storage, texture),
                 *$_c(Storage, shader);

extern $$c(SceneSet) *$_c(SceneSet, default);

void $g(Texture, free)($$g(Texture) *texture);
$$c(SceneSet) *$c(SceneSet, create)();
void $c(SceneSet, free)($$c(SceneSet) *set);

int $c(Context, process)($$c(Context) *context)
{
    {
        int status = context->processor->process_init ? context->processor->process_init(context) : 0;

        if (status) {
            pthread_mutex_lock(&$_c(Context, statusMutex));
            context->status = status;
            pthread_mutex_unlock(&$_c(Context, statusMutex));
            return status;
        }
    }

    int status;
    do {
        $_t(uhuge) time_before = $c(Time, now)();
        $p(Handle, onProcess)();
        status = context->processor->process(context);
        $_t(uhuge) time_cost = $c(Time, now)() - time_before;
        if (time_cost < proc_time) {
            $c(Time, sleep)(proc_time - time_cost);
        }
    } while (context->status == -1 && status == -1);

    status = context->processor->process_free ? context->processor->process_free(context) : 0;

    $c(List, free)($_p(Handler, onProcess));

    return status;
}

int $c(Context, respond)($$c(Context) *context)
{
    int status = -1;
    XEvent event;
    do {
        while (XPending(context->display)) {
            XNextEvent(context->display, &event);
            pthread_mutex_lock(&$_c(Context, displayMutex));

            switch (event.type) {
                case ClientMessage:
                    if (event.xclient.data.l[0] == *(Atom*)context->attr[1]) {
                        status = 0;
                    }
                    break;case ConfigureNotify:
                    {
                        XConfigureEvent xce = event.xconfigure;
                        if (context->width != xce.width || context->height != xce.height) {
                            context->width = xce.width;
                            context->height = xce.height;
                            if (context->processor->resize) {
                                context->processor->resize(context->width, context->height);
                            }
                        }
                    }
                    break;
                case KeyPress:
                    {
                        unsigned char ascKey = $c(Key, toAsc)(event.xkey.keycode);

                        if (ascKey) {
                            alloc_array(void*, argv, 2);
                            argv[0] = context;
                            argv[1] = new(unsigned char);
                            *(unsigned char*)argv[1] = ascKey;
                            $p(Call, onProcess)($p(Core, handleKeyDown), 2, argv);
                        }
                    }
                    break;
                case KeyRelease:
                    {
                        _Bool release = 1;
                        if (XEventsQueued(context->display, QueuedAfterReading)) {
                            XEvent nev;
                            XPeekEvent(context->display, &nev);
                            if (
                                nev.type == KeyPress &&
                                nev.xkey.keycode == event.xkey.keycode &&
                                nev.xkey.time == event.xkey.time
                            ) {
                                release = 0;
                            }
                        }
                        if (release) {
                            unsigned char ascKey = $c(Key, toAsc)(event.xkey.keycode);

                            if (ascKey) {
                                alloc_array(void*, argv, 2);
                                argv[0] = context;
                                argv[1] = new(unsigned char);
                                *(unsigned char*)argv[1] = ascKey;
                                $p(Call, onProcess)($p(Core, handleKeyUp), 2, argv);
                            }
                        }
                    }
                    break;
                case ButtonPress:
                    {
                        alloc_array(void*, argv, 4);
                        argv[0] = context;
                        argv[1] = new($_t(uhuge));
                        *($_t(uhuge)*)argv[1] = (event.xbutton.x < 0 ? 0 : event.xbutton.x);
                        argv[2] = new($_t(uhuge));
                        *($_t(uhuge)*)argv[2] = (event.xbutton.y < 0 ? 0 : event.xbutton.y);
                        argv[3] = new(unsigned char);
                        *(unsigned char*)argv[3] = event.xbutton.button - 1;
                        $p(Call, onProcess)($p(Core, handlePointerDown), 4, argv);
                    }
                    break;
                case ButtonRelease:
                    {
                        alloc_array(void*, argv, 4);
                        argv[0] = context;
                        argv[1] = new($_t(uhuge));
                        *($_t(uhuge)*)argv[1] = (event.xbutton.x < 0 ? 0 : event.xbutton.x);
                        argv[2] = new($_t(uhuge));
                        *($_t(uhuge)*)argv[2] = (event.xbutton.y < 0 ? 0 : event.xbutton.y);
                        argv[3] = new(unsigned char);
                        *(unsigned char*)argv[3] = event.xbutton.button - 1;
                        $p(Call, onProcess)($p(Core, handlePointerUp), 4, argv);
                    }
                    break;
                case MotionNotify:
                    {
                        alloc_array(void*, argv, 3);
                        argv[0] = context;
                        argv[1] = new($_t(uhuge));
                        *($_t(uhuge)*)argv[1] = (event.xbutton.x < 0 ? 0 : event.xbutton.x);
                        argv[2] = new($_t(uhuge));
                        *($_t(uhuge)*)argv[2] = (event.xbutton.y < 0 ? 0 : event.xbutton.y);
                        $p(Call, onProcess)($p(Core, handlePointerMove), 3, argv);
                    }
                    break;
            }
            pthread_mutex_unlock(&$_c(Context, displayMutex));
        }
        //$c(Context, sleep)(proc_time);
    } while (context->status == -1 && status == -1);

    status = context->processor->respond_free ? context->processor->respond_free(context) : 0;

    return status;
}

int $c(Context, render)($$c(Context) *context)
{
    {
        int status = context->processor->render_init ? context->processor->render_init(context) : 0;

        if (status) {
            pthread_mutex_lock(&$_c(Context, statusMutex));
            context->status = status;
            pthread_mutex_unlock(&$_c(Context, statusMutex));
            return status;
        }
    }

    int status;
    do {
        $_t(uhuge) time_before = $c(Time, now)();
        pthread_mutex_lock(&$_c(Context, displayMutex));
        $g(Handle, onRender)();
        status = context->processor->render(context);
        pthread_mutex_unlock(&$_c(Context, displayMutex));
        $_t(uhuge) time_cost = $c(Time, now)() - time_before;
        if (time_cost < frame_time) {
            $c(Time, sleep)(frame_time - time_cost);
        }
    } while (context->status == -1 && status == -1);

    status = context->processor->render_free ? context->processor->render_free(context) : 0;

    if (context->status == 1) {
        pthread_mutex_lock(&$_c(Context, displayMutex));
        context->status = status;
        pthread_mutex_unlock(&$_c(Context, displayMutex));
    }
    $c(List, free)($_g(Handler, onRender));

    return status;
}

int $c(Context, create)(_Bool fullscreen, $$c(UPoint) size, const char *name, $$c(ContextProcessor) processor)
{
    $_c(Object, entries) = $c(List, create)();
    $_p(Handler, onProcess) = $c(List, create)();
    $_g(Handler, onRender) = $c(List, create)();
    $_c(Storage, texture) = $c(List, create)();
    $_c(Storage, rhg) = $c(List, create)();
    $_c(Storage, shader) = $c(List, create)();

    alloc($$c(Context), context);

    context->keys = new_array(_Bool, 256);
    context->pointers = $c(List, create)();
    context->sceneSet = $c(SceneSet, create)();

    $c(Pointer, add)(context->pointers, 0, 0, 0);
    context->processor = &processor;
    context->title = (char *) name;

    XInitThreads();

    context->fullscreen = fullscreen;
    context->display = XOpenDisplay(NULL);
    context->status = -1;
    if (context->display == NULL) {
        return 1;
    }
    context->screen = DefaultScreen(context->display);
    context->window =
        XCreateSimpleWindow(
            context->display,
            RootWindow(
                context->display,
                context->screen
            ),
            0,
            0,
            size.x,
            size.y,
            1,
            BlackPixel(context->display, context->screen),
            BlackPixel(context->display, context->screen)
        );

    if (fullscreen) {
        Atom atoms[2] = {XInternAtom(context->display, "_NET_WM_STATE_FULLSCREEN", False), None};
        XChangeProperty(
            context->display,
            context->window,
            XInternAtom(context->display, "_NET_WM_STATE", False),
            XA_ATOM, 32, PropModeReplace, (const unsigned char *) atoms, 1
        );
    }
    else {
        XStoreName(context->display, context->window, context->title);
        Xutf8SetWMProperties(context->display, context->window, context->title, NULL, NULL, 0, NULL, NULL, NULL);
    }

    XMapWindow(context->display, context->window);

    context->attr = new_array(void*, 2);

    alloc(Atom, delete_atom);

    *delete_atom = XInternAtom(context->display, "WM_DELETE_WINDOW", False);
    XSetWMProtocols(context->display, context->window, delete_atom, 1);

    XSelectInput(
        context->display,
        context->window,
        KeyPressMask
        | KeyReleaseMask
        | StructureNotifyMask
        | PointerMotionMask
        | ButtonMotionMask
        | ButtonPressMask
        | ButtonReleaseMask
    );

    context->attr[1] = (void*)delete_atom;

    context->status = context->processor->respond_init ? context->processor->respond_init(context) : 0;

    if (context->status == 0) {
        context->status = -1;

        pthread_t proc_tid, ren_tid;

        pthread_create(&proc_tid, NULL, (void *) $c(Context, process), context);
        pthread_create(&ren_tid, NULL, (void *) $c(Context, render), context);

        context->status = $c(Context, respond)(context);

        context->status = context->status ?: pthread_join(proc_tid, NULL);
        context->status = context->status ?: pthread_join(ren_tid, NULL);

        XDestroyWindow(context->display, context->window);
        XCloseDisplay(context->display);
    }

    int result = context->status;

    free(delete_atom);
    free(context->attr);
    free(context->keys);
    $c(SceneSet, free)(context->sceneSet);
    $each ($$c(Pointer), pointer, context->pointers) {
        $c(Pointer, free)(pointer);
    }
    $c(List, free)(context->pointers);
    free(context);

    $c(Storage, free)($_c(Storage, shader));
    {
        $each($$c(Item), item, $_c(Storage, rhg)) {
            $g(Rhg, free)(item->value);
        }
    }
    $c(Storage, free)($_c(Storage, rhg));
    {
        $each($$c(Item), item, $_c(Storage, texture)) {
            $g(Texture, free)(item->value);
        }
    }
    $c(Storage, free)($_c(Storage, texture));
    $c(Object, freeEntries)();

    return result;
}

unsigned char $c(Key, toAsc)(unsigned int keyCode)
{
    for ($_t(uhuge) i = 0; i < $rhic_keymap_size; i += 2) {
        if ($rhic_keymap[i] == keyCode) {
            return $rhic_keymap[i + 1];
        }
    }

    return 0;
}

#endif //__unix__
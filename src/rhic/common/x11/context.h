/**
 * X11 implementation of Rhic Context
 */

#ifdef __unix__
#ifndef RHIC_X11_CONTEXT_H
#define RHIC_X11_CONTEXT_H

#include <X11/Xlib.h>

#include "../primitive.h"
#include "../all/context.h"

$$c(SceneSet);

#define $rhic_keymap_size 200
#define $rhic_keymap \
(const int[]){\
    0,  0, \
    22, 8, \
    23, 9, \
    36, 13,\
    \
    104,13,\
    50, 16,\
    62, 16,\
    37, 17,\
    \
    105,17,\
    64, 18,\
    108,18,\
    127,19,\
    \
    66, 20,\
    9,  27,\
    65, 32,\
    112,33,\
    \
    117,34,\
    115,35,\
    110,36,\
    113,37,\
    \
    111,38,\
    114,39,\
    116,40,\
    107,44,\
    \
    118,45,\
    119,46,\
    19, 48,\
    10, 49,\
    \
    11, 50,\
    12, 51,\
    13, 52,\
    14, 53,\
    \
    15, 54,\
    16, 55,\
    17, 56,\
    18, 57,\
    \
    38, 65,\
    56, 66,\
    54, 67,\
    40, 68,\
    \
    26, 69,\
    41, 70,\
    42, 71,\
    43, 72,\
    \
    31, 73,\
    44, 74,\
    45, 75,\
    46, 76,\
    \
    58, 77,\
    57, 78,\
    32, 79,\
    33, 80,\
    \
    24, 81,\
    27, 82,\
    39, 83,\
    28, 84,\
    \
    30, 85,\
    55, 86,\
    25, 87,\
    53, 88,\
    \
    29, 89,\
    52, 90,\
    90, 96,\
    87, 97,\
    \
    88, 98,\
    89, 99,\
    83, 100,\
    84, 101,\
    85, 102,\
    \
    79, 103,\
    80, 104,\
    81, 105,\
    63, 106,\
    \
    86, 107,\
    82, 109,\
    91, 110,\
    106,111,\
    \
    67, 112,\
    68, 113,\
    69, 114,\
    70, 115,\
    \
    71, 116,\
    72, 117,\
    73, 118,\
    74, 119,\
    \
    75, 120,\
    95, 121,\
    96, 122,\
    77, 144,\
    \
    78, 145,\
    47, 186,\
    21, 187,\
    59, 188,\
    \
    20, 189,\
    60, 190,\
    61, 191,\
    34, 219,\
    \
    49, 220,\
    35, 221,\
    51, 222,\
    94, 222,\
}

$$c(Context)
{
    Display *display;
    Window window;
    int screen;
    char *title;
    int status;
    _Bool fullscreen;
    $$c(ContextProcessor) *processor;
    void** attr;
    $_t(uhuge) width, height;
    $$c(List) *pointers;
    _Bool *keys;
    $$c(SceneSet) *sceneSet;
};

int $c(Context, create)(_Bool fullscreen, $$c(UPoint) size, const char *name, $$c(ContextProcessor) processor);

int $c(Context, process)($$c(Context) *context);
int $c(Context, respond)($$c(Context) *context);
int $c(Context, render)($$c(Context) *context);

unsigned char $c(Key, toAsc)(unsigned int keyCode);

#endif //RHIC_X11_CONTEXT_H
#endif //__unix__
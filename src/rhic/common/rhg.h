/**
 * RHG is an implementation of an image format that contains image as
 * a divided structure of an index table and multiple palettes.
 *
 * First byte describes index table layout.
 * 0x00 - each cell of index table equals to one pixel
 * 0x01 - first cell in a pair tells value of index, second tells how many pixels of this index are in a row
 *
 * Second byte tells the size of a cell of index table. Max size is 4 bytes.
 *
 * Bytes #3-6 tell image width
 * Bytes #7-10 tell image height
 *
 * Bytes #11-14 tells how many palettes the object has
 * Bytes #15-18 tells how many colors are used to describe an image
 *
 * Next goes index table. Its sizing is controlled by bytes #1 and #2.
 * Then goes palette table. Each color is described in 4 bytes (RGBA)
 */
#ifndef RHIC_GL_RHG_H
#define RHIC_GL_RHG_H

#include "macro.h"

$$c(List);

$$g(Rhg)
{
    unsigned char **palette;
    $_t(uhuge) palette_num;
    $_t(uhuge) width;
    $_t(uhuge) *data;
    $_t(uhuge) data_size;
};

$$c(List) *$_c(Storage, rhg);


/**
 * Gets an rhg object by its name
 * @param name
 * @return
 */

$$g(Rhg) *$g(Rhg, get)(char *name);


/**
 * Loads an rhg object from a byte array
 * @param content
 * @return
 */

$$g(Rhg) *$g(Rhg, load)(unsigned char *content);


/**
 * Applies a palette to an index table
 * @param rhg
 * @param index
 * @return
 */

unsigned char *$g(Rhg, apply)($$g(Rhg) *rhg, $_t(uhuge) index);


/**
 * Frees an rhg object
 */

void $g(Rhg, free)($$g(Rhg) *rhg);

#endif //RHIC_GL_RHG_H

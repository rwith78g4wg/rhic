#ifndef RHIC_COMMON_EVENT_HANDLER_H
#define RHIC_COMMON_EVENT_HANDLER_H

#include "macro.h"

$$c(List);


/**
 * Event handler structure. It must handler function.
 * Also it may have its own arguments which will be appended to
 */
$$c(EventHandler) {
    int argc;
    void **argv;
    void *(*handler)(int argc, void **argv);
};


/**
 * Event handler group structure. It must have type which describes which events the group contains.
 */

$$c(EventHandlerGroup) {
    $_t(uhuge) eventType;
    $$c(List) *eventHandlers;
};


/**
 * Creates unattached event handler
 *
 * @param handler Handler function
 * @param argc Number of handler arguments
 * @param argv Handler arguments
 */

$$c(EventHandler) *$c(EventHandler, create)(void *(*handler)(int argc, void **argv), int argc, void **argv);


/**
 * Registers new event handler to handler groups
 *
 * @param eventGroups Handler groups where to register new handler
 * @param eventType Type of event to handle
 * @param handler Handler function
 * @param argc Number of handler arguments
 * @param argv Handler arguments
 */

void $c(EventHandler, register)($$c(List) *eventGroups, $_t(uhuge) eventType, void *(*handler)(int argc, void **argv), int argc, void **argv);


/**
 * Fires an event of given type calling all same type handlers from handler groups
 */

void $c(EventHandler, fire)($$c(List) *eventGroups, $_t(uhuge) eventType, int argc, void **argv);

#endif //RHIC_COMMON_EVENT_HANDLER_H

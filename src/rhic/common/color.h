#ifndef RHIC_COMMON_COLOR_H
#define RHIC_COMMON_COLOR_H

#include "macro.h"

#define RGBA(var, r, g, b, a) var[0] = r;var[1] = g;var[2] = b;var[3] = a

void $c(Color, brightenBy)(float *source, float *dest, float times);
void $c(Color, saturateBy)(float *source, float *dest, float times);

#endif //RHIC_COMMON_COLOR_H

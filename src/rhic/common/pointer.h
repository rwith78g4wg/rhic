#ifndef RHIC_COMMON_POINTER_H
#define RHIC_COMMON_POINTER_H

#include "macro.h"

$$c(List);

$$c(Pointer) {
    double x, y;
    unsigned char id;
    _Bool *button;
};


/**
 * Adds new pointer to pointers list
 * @param pointers List of existing pointers
 * @param id Id of a new pointer
 * @param x X coord
 * @param y Y coord
 * @return If there is a pointer of given id, returns it. Else creates and returns new pointer
 */

$$c(Pointer) *$c(Pointer, add)($$c(List) *pointers, unsigned char id, double x, double y);


/**
 * Frees a pointer
 * @param pointer
 */

void $c(Pointer, free)($$c(Pointer) *pointer);


/**
 * Searches for a pointer to be contained inside a rectangle
 * @param pointers List of pointers
 * @param button Filter by pressed button of the pointer. If not needed, set to 255
 * @param left Left border of a rectangle
 * @param top Top border of a rectangle
 * @param right Right border of a rectangle
 * @param bottom Bottom border of a rectangle
 * @return
 */

$$c(Pointer) *$c(Pointer, findContained)($$c(List) *pointers, unsigned char button, double left, double top, double right, double bottom);


/**
 * Searches for a pointer of a given id
 * @param pointers
 * @param id
 * @return
 */

$$c(Pointer) *$c(Pointer, findById)($$c(List) *pointers, unsigned char id);


/**
 * Searches for a pointer using given callback. Callback must return true when the pointer fits the rule.
 * @param pointers
 * @param callback
 * @return
 */

$$c(Pointer) *$c(Pointer, findByRule)($$c(List) *pointers, _Bool (*callback)($$c(Pointer)* pointer));

#endif //RHIC_COMMON_POINTER_H

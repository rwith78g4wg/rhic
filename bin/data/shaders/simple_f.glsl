#if __VERSION__ < 130
#define texture2d texture2D
#else
#define texture2d texture
#endif

uniform vec4 uColor;
uniform sampler2D uTexture0;
varying vec2 var_TexCoord;

void main() {
    if (uColor.w == 0.0) {
        discard;
    }
    if (uColor.w > 0.0) {
        gl_FragColor = uColor * texture2d(uTexture0, var_TexCoord);
    } else {
        gl_FragColor = vec4(uColor.xyz, -uColor.w);
    }
    if (gl_FragColor.w == 0.0) {
        discard;
    }
}

attribute vec3 at_Position;
attribute vec2 at_TexCoord;

uniform vec4 uRotate;
uniform vec4 uTranslate;
uniform vec4 uScale;

uniform mat4 uProjectionMatrix;
uniform mat4 uViewMatrix;

varying vec2 var_TexCoord;

mat4 fnRotate(vec4 vcRotate)
{
    if (vcRotate.xyz == vec3(0.0)) {
        return mat4(
            vec4(1.0, 0.0, 0.0, 0.0),
            vec4(0.0, 1.0, 0.0, 0.0),
            vec4(0.0, 0.0, 1.0, 0.0),
            vec4(0.0, 0.0, 0.0, 1.0)
        );
    }
    vcRotate.xyz = normalize(vcRotate.xyz);
    float s = sin(vcRotate.w);
    float c = cos(vcRotate.w);
    float oc = 1.0 - c;
	return mat4(
	    oc * vcRotate.x * vcRotate.x + c, oc * vcRotate.x * vcRotate.y - vcRotate.z * s, oc * vcRotate.x * vcRotate.z + vcRotate.y * s, 0.0,
        oc * vcRotate.x * vcRotate.y + vcRotate.z * s, oc * vcRotate.y * vcRotate.y + c, oc * vcRotate.y * vcRotate.z - vcRotate.x * s, 0.0,
        oc * vcRotate.x * vcRotate.z - vcRotate.y * s, oc * vcRotate.y * vcRotate.z + vcRotate.x * s, oc * vcRotate.z * vcRotate.z + c, 0.0,
        0.0, 0.0, 0.0, 1.0
    );
}

mat4 fnRegularRotate(vec4 vcRotate)
{
    vec3 vcSin = sin(vcRotate.xyz * vcRotate.w);
    vec3 vcCos = cos(vcRotate.xyz * vcRotate.w);
    return mat4(
        vcCos.y * vcCos.z,
        vcCos.x * vcSin.z + vcSin.x * vcSin.y * vcCos.z,
        vcSin.y * vcSin.z - vcCos.x * vcSin.y * vcCos.z,
        0.0,
        
        -vcCos.y * vcSin.z,
        vcCos.x * vcCos.z - vcSin.x * vcSin.y * vcSin.z,
        vcSin.x * vcCos.z + vcCos.x * vcSin.x * vcSin.z,
        0.0,
        
        vcSin.y,
        vcSin.x * vcCos.x,
        vcCos.x * vcCos.y,
        0.0,
        
        0.0,
        0.0,
        0.0,
        1.0
    );
}

mat4 fnRegularRotateO(vec4 vcRotate)
{
    float s = sin(vcRotate.w);
    float c = cos(vcRotate.w);
    return mat4(
        vcRotate.x + c * (vcRotate.y + vcRotate.z),
        -s * (vcRotate.z),
        s * (vcRotate.y),
        0,

        s * (vcRotate.z),
        c * (vcRotate.x + vcRotate.y),
        -s * (vcRotate.x),
        0,

        -s * (vcRotate.y),
        s * (vcRotate.x),
        vcRotate.z + c * (vcRotate.x + vcRotate.y),
        0,
        
        0,
        0,
        0,
        1
    );
}

mat4 fnTranslate(vec4 vcTranslate)
{
    return mat4(
        vec4(1.0, 0.0, 0.0, 0.0),
        vec4(0.0, 1.0, 0.0, 0.0),
        vec4(0.0, 0.0, 1.0, 0.0),
        vec4(vcTranslate.x, -vcTranslate.y, vcTranslate.z, vcTranslate.w)
    );
}

mat4 fnScale(vec4 vcScale)
{
    return mat4(
        vec4(vcScale.x, 0.0, 0.0, 0.0),
        vec4(0.0, vcScale.y, 0.0, 0.0),
        vec4(0.0, 0.0, vcScale.z, 0.0),
        vec4(0.0, 0.0, 0.0, vcScale.w)
    );
}

void main() {
	gl_Position = uProjectionMatrix
	            * uViewMatrix
                * fnTranslate(uTranslate)
                * fnRegularRotate(uRotate)
                * fnScale(uScale)
	            * vec4(at_Position, 1.0)
	            ;

	var_TexCoord = at_TexCoord;
}
